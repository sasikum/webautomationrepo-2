package com.tatahealth.EMR.pages.RegisterPatient;

import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;



public class PatientRegisterPage {
	
	public String getAlertText(WebDriver driver,ExtentTest logger) 
	{
		//String alert = Web_GeneralFunctions.findElementbyClassName("popover", driver, logger).getText(); 
		String alert = driver.findElement(By.className("popover")).getText();
		// fade top in popover-danger
		return alert;	
	}
	
	public WebElement getTitleDropDown(WebDriver driver,ExtentTest logger) 
	{
		WebElement element = Web_GeneralFunctions.findElementbySelector("#title",driver,logger);
		return element;	
	}
	
	public WebElement setTitlefromDropdown(WebDriver driver,ExtentTest logger) 
	{
		String n = "0";
		while(n.equalsIgnoreCase("0")||n.equalsIgnoreCase("1")) {
			n = RandomStringUtils.randomNumeric(1);
		}
		WebElement element = Web_GeneralFunctions.findElementbySelector("#title > option:nth-child("+n+")",driver,logger);
		//System.out.println(n);
		return element;	
	}
	
	
	public WebElement getNameTextBox(WebDriver driver,ExtentTest logger) 
	{
		WebElement element = Web_GeneralFunctions.findElementbySelector("#patientName",driver,logger);
		return element;	
	}
	
	
	public WebElement getGenderDropDown(WebDriver driver,ExtentTest logger) 
	{
		WebElement username = Web_GeneralFunctions.findElementbySelector("#gender",driver,logger);
		return username;	
	}
	
	public WebElement setGenderfromDropdown(WebDriver driver,ExtentTest logger) 
	{
		WebElement username = Web_GeneralFunctions.findElementbySelector("#gender > option:nth-child(3)",driver,logger);
		return username;	
	}
	
	
	public WebElement getAgeinYears(WebDriver driver,ExtentTest logger) 
	{
		WebElement element = Web_GeneralFunctions.findElementbySelector("#age",driver,logger);
		return element;	
	}
	
	public WebElement getAgeinMonths(WebDriver driver,ExtentTest logger) 
	{
		WebElement element = Web_GeneralFunctions.findElementbySelector("#ageInMonths",driver,logger);
		return element;	
	}
	
	public WebElement getAgeinDays(WebDriver driver,ExtentTest logger) 
	{
		WebElement element = Web_GeneralFunctions.findElementbySelector("#ageInDays",driver,logger);
		return element;	
	}
	
	public WebElement getDOBValue(WebDriver driver,ExtentTest logger) 
	{
		WebElement element = Web_GeneralFunctions.findElementbySelector("#dob",driver,logger);
		return element;	
	}
	
	
	public WebElement setMobileNo(WebDriver driver,ExtentTest logger) 
	{
		WebElement element = Web_GeneralFunctions.findElementbySelector("#mobileNo",driver,logger);
		return element;	
	}
	
	public WebElement setMobileNoNA(WebDriver driver,ExtentTest logger) 
	{
		WebElement username = Web_GeneralFunctions.findElementbySelector("#patientAddForm > div.container-fluid > div > div.marbtnptient > div.row > div.col-lg-9 > div > div:nth-child(1) > div.col-md-5 > div.pull-right > input",driver,logger);
		return username;	
	}
	
	public WebElement setAllergiesNil(WebDriver driver,ExtentTest logger) 
	{
		WebElement username = Web_GeneralFunctions.findElementbySelector("#allergyNilCheck",driver,logger);
		return username;	
	}
	
	
	public WebElement sweetAlertConfirm(WebDriver driver,ExtentTest logger) 
	{
		WebElement username = Web_GeneralFunctions.findElementbySelector("body > div.sweet-alert.showSweetAlert.visible > div.sa-button-container > div > button",driver,logger);
		return username;	
	}
	
	
	public WebElement sweetAlertCancel(WebDriver driver,ExtentTest logger) 
	{
		WebElement username = Web_GeneralFunctions.findElementbySelector("body > div.sweet-alert.showSweetAlert.visible > div.sa-button-container > button",driver,logger);
		return username;	
	}
	
	
	public WebElement getAllergies(WebDriver driver,ExtentTest logger) 
	{
		WebElement username = Web_GeneralFunctions.findElementbySelector("#tags_2_tagsinput",driver,logger);
		return username;	
	}

	
	public WebElement setAllergies(WebDriver driver,ExtentTest logger) 
	{
		WebElement username = Web_GeneralFunctions.findElementbySelector("#tags_2_tag",driver,logger);
		return username;	
	}
	
	
	public WebElement getSocialHabits(int i, WebDriver driver,ExtentTest logger) 
	{
		WebElement username = Web_GeneralFunctions.findElementbySelector("#socialHabits"+i,driver,logger);
		return username;	
	}
	
	
	public WebElement getSocialHabitsDuration(int i, WebDriver driver,ExtentTest logger) 
	{
		WebElement username = Web_GeneralFunctions.findElementbySelector("#socialHabitsDuration"+i,driver,logger);
		return username;	
	}
	
	public WebElement getSocialHabitsDurationType(int i, int j, WebDriver driver,ExtentTest logger) 
	{
		WebElement username = Web_GeneralFunctions.findElementbySelector("#socialhabitsDurationUnit"+i+" > option:nth-child("+j+")",driver,logger);
		return username;	
	}
	
	public WebElement getSocialHabitsComments(int i, WebDriver driver,ExtentTest logger) 
	{
		WebElement username = Web_GeneralFunctions.findElementbySelector("#socialHabitsComment"+i,driver,logger);
		return username;	
	}
	
	
	public WebElement addSocialHabitsRow(int i, WebDriver driver,ExtentTest logger) 
	{
		String a = "#socialHabitRow"+i+" > td.actions-td > button.btn.btn-success.addSocialHabitBtn";
		WebElement username = Web_GeneralFunctions.findElementbySelector(a,driver,logger);
		return username;	
	}
	
	
	public WebElement deleteSocialHabitsRow(int i, WebDriver driver,ExtentTest logger) 
	{
		String a = "#socialHabitRow"+i+" > td.actions-td > button";
		WebElement username = Web_GeneralFunctions.findElementbySelector(a,driver,logger);
		return username;	
	}
	
	public WebElement setSocialHabitsNil(WebDriver driver,ExtentTest logger) 
	{
		WebElement username = Web_GeneralFunctions.findElementbySelector("#socialHabitsNilCheck",driver,logger);
		return username;	
	}
	
	
	
	public WebElement getEmailIdTextbox(WebDriver driver,ExtentTest logger) 
	{
		WebElement username = Web_GeneralFunctions.findElementbySelector("#emailID",driver,logger);
		return username;	
	}
	
	
	public WebElement getMoreBtn(WebDriver driver,ExtentTest logger) 
	{
		WebElement username = Web_GeneralFunctions.findElementbySelector("#openMoreField",driver,logger);
		return username;	
	}
	
	
	public WebElement getReligionSelector(WebDriver driver,ExtentTest logger) 
	{
		WebElement username = Web_GeneralFunctions.findElementbySelector("#religion",driver,logger);
		return username;	
	}
	
	
	public WebElement setOtherReligion(WebDriver driver,ExtentTest logger) 
	{
		WebElement username = Web_GeneralFunctions.findElementbySelector("#otherReligionValue",driver,logger);
		return username;	
	}
	
	
	public WebElement getGovtIdTypeSelector(WebDriver driver,ExtentTest logger) 
	{
		WebElement username = Web_GeneralFunctions.findElementbySelector("#government_id_type",driver,logger);
		return username;	
	}
	
	
	public WebElement setRandomGovtIdType(WebDriver driver,ExtentTest logger) 
	{
		Random rand = new Random();
		int n = rand.nextInt(8);
		while(n==0&&n==1) {
			n = rand.nextInt(8);
		}
		WebElement username = Web_GeneralFunctions.findElementbySelector("#government_id_type > option:nth-child("+n+")",driver,logger);
		return username;	
	}
	
	public WebElement setAadharGovtIdType(WebDriver driver,ExtentTest logger) 
	{
		WebElement username = Web_GeneralFunctions.findElementbySelector("#government_id_type > option:nth-child(6)",driver,logger);
		return username;	
	}
	
	public WebElement setGovtIdNumber(WebDriver driver,ExtentTest logger) 
	{
		WebElement username = Web_GeneralFunctions.findElementbySelector("#government_id_number",driver,logger);
		return username;	
	}
	
	
	public WebElement getMlcYesRadioBtn(WebDriver driver,ExtentTest logger) 
	{
		WebElement username = Web_GeneralFunctions.findElementbySelector("#mlcYes",driver,logger);
		return username;	
	}
	
	public WebElement getMlcNumberTextbox(WebDriver driver,ExtentTest logger) 
	{
		WebElement username = Web_GeneralFunctions.findElementbySelector("#mlc_number",driver,logger);
		return username;	
	}
	
	public WebElement getMlcDescTextBox(WebDriver driver,ExtentTest logger) 
	{
		WebElement username = Web_GeneralFunctions.findElementbySelector("#mlc_description",driver,logger);
		return username;	
	}
	
	
	public WebElement getOtherAsReligion(WebDriver driver,ExtentTest logger) 
	{
		WebElement username = Web_GeneralFunctions.findElementbySelector("#religion > option:nth-child(6)",driver,logger);
		return username;	
	}
	
	
	public WebElement getSaveBtn(WebDriver driver,ExtentTest logger) 
	{
		WebElement username = Web_GeneralFunctions.findElementbySelector("#btnMedical",driver,logger);
		return username;	
	}
	
	
	
	
	
}
package com.tatahealth.EMR.pages.Login;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class ChooseClinicPage {
	
	public boolean leadChooseClinicPage(WebDriver driver,ExtentTest logger) {
		boolean getUrl = false;
		try {
			String currentUrl = driver.getCurrentUrl();
			if(currentUrl.contains("/chooseClinic")) {
				getUrl = true;
			}
			
		}catch(Exception e) {
			return false;
		}
		return getUrl;
	}
	
	public WebElement clickOrg(WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//div[contains(text(),'Qa Test Clinic Org')]", driver, logger);
		return element;
	}
}
package com.tatahealth.EMR.pages.UserAdministration;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class UserAdministration {
	
	public WebDriver driver;
	
	// ************************************************* create page ************************************************//

	public WebElement getEmrMenu(WebDriver driver,ExtentTest logger) 
	{
		WebElement emrMenu = Web_GeneralFunctions.findElementbyXPath("//a[@id='menu-toggle']//img", driver, logger);
 		return emrMenu;	
	}

	public WebElement getUserAdministrationLnk(WebDriver driver,ExtentTest logger) 
	{
		WebElement userAdministration = Web_GeneralFunctions.findElementbyXPath("//a[contains(text(),'User Administration')]", driver, logger);
 		return userAdministration;	
	}
	
	public WebElement GetAddNewUserLnk(WebDriver driver,ExtentTest logger) 
	{
		WebElement addNewUser = Web_GeneralFunctions.findElementbyXPath("//a[contains(text(),'Add New User')]", driver, logger);
 		return addNewUser;	
	}
	
	public WebElement getPersonalInformationLbl(WebDriver driver,ExtentTest logger) 
	{
		WebElement personalInformation = Web_GeneralFunctions.findElementbyXPath("//h4[contains(text(),'Personal Information')]", driver, logger);
 		return personalInformation;	
	}

    public WebElement getTitleDdl(WebDriver driver,ExtentTest logger) 
	{
		WebElement title = Web_GeneralFunctions.findElementbySelector("#title",driver,logger);
			return title;	
	}
    
    public WebElement getNameTextBox(WebDriver driver,ExtentTest logger) 
	{
		WebElement name = Web_GeneralFunctions.findElementbySelector("#userName",driver,logger);
			return name;	
	}
    
    public WebElement getGenderDdl(WebDriver driver,ExtentTest logger) 
	{
		WebElement gender = Web_GeneralFunctions.findElementbySelector("#gender",driver,logger);
			return gender;	
	}
	
    public WebElement getAddressTextBox(WebDriver driver,ExtentTest logger) 
	{
		WebElement address = Web_GeneralFunctions.findElementbySelector("#address",driver,logger);
			return address;	
	}
	
    
    public WebElement getDobCalender(WebDriver driver,ExtentTest logger) 
	{
		WebElement doB = Web_GeneralFunctions.findElementbySelector("#userdob",driver,logger);
			return doB;	
	}
	
    public WebElement getroleTypeDdl(WebDriver driver,ExtentTest logger) 
	{
		WebElement roleType = Web_GeneralFunctions.findElementbySelector("#roleType",driver,logger);
			return roleType;	
	}
	
    public WebElement getMobileNumberTextBox(WebDriver driver,ExtentTest logger) 
	{
		WebElement mobileNumber = Web_GeneralFunctions.findElementbySelector("#mobileNumber",driver,logger);
			return mobileNumber;	
	}
	
    public WebElement getbloodGroupDdl(WebDriver driver,ExtentTest logger) 
	{
		WebElement bloodGroup = Web_GeneralFunctions.findElementbySelector("#bloodGroup",driver,logger);
			return bloodGroup;	
	}
	
    public WebElement getEmailTextBox(WebDriver driver,ExtentTest logger) 
	{
		WebElement email = Web_GeneralFunctions.findElementbySelector("#email",driver,logger);
			return email;	
	}
    public WebElement getClinicServicesTextBox(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement clinicServices = Web_GeneralFunctions.findElementbySelector("#clinicServices",driver,logger);
   			return clinicServices;	
   	}
    
    public WebElement getMultiSpecialityBtn(WebDriver driver,ExtentTest logger) 
	{
		WebElement speciality = Web_GeneralFunctions.findElementbyXPath("//button[@id='multi_speciality_btn']", driver, logger);
 		return speciality;	
	}
    // below are the options of Speciality button
    public WebElement getAndrologistCbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement andrologist = Web_GeneralFunctions.findElementbyXPath("//label[contains(text(),'Andrologist')]", driver, logger);
    		return andrologist;	
   	}
     
    public WebElement getAYUSHCbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement AYUSH = Web_GeneralFunctions.findElementbyXPath("//label[contains(text(),'AYUSH')]", driver, logger);
    		return AYUSH;	
   	}
    
    //*****************************************************************End **********************************************//
    public WebElement getMedicalRegNoTextBox(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement medicalRegNo = Web_GeneralFunctions.findElementbySelector("#medicalRegNo",driver,logger);
   			return medicalRegNo;	
   	}
    
    public WebElement getOperationRoleListMulBtn (WebDriver driver,ExtentTest logger) 
	{
		WebElement operationRoleList  = Web_GeneralFunctions.findElementbyXPath("//div[contains(@class,'col-xs-8')]//button[@class='multiselect dropdown-toggle btn btn-default']", driver, logger);
 		return operationRoleList;	
	}
    // *********************************************** below are the elements/options of oparation location list ***************************//
    public WebElement getParamedicCbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement paramedic = Web_GeneralFunctions.findElementbyXPath("//label[contains(text(),'Paramedic')]", driver, logger);
    		return paramedic;	
   	}
    public WebElement getClinicAdminCbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement clinicAdmin = Web_GeneralFunctions.findElementbyXPath("//label[contains(text(),'Clinic Admin')]", driver, logger);
    		return clinicAdmin;	
   	}
    public WebElement getchiefDoctorCbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement chiefDoctor = Web_GeneralFunctions.findElementbyXPath("//label[contains(text(),'Chief Doctor')]", driver, logger);
    		return chiefDoctor;	
   	}
    public WebElement getReceptionistCbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement Receptionist = Web_GeneralFunctions.findElementbyXPath("//label[contains(text(),'Receptionist')]", driver, logger);
    		return Receptionist;	
   	}
    public WebElement getBillingDeskCbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement billingDesk = Web_GeneralFunctions.findElementbyXPath("//label[contains(text(),'Billing Desk')]", driver, logger);
    		return billingDesk;	
   	}
    public WebElement getCallCentreNurseCbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement CallCentreNurse = Web_GeneralFunctions.findElementbyXPath("//label[contains(text(),'Call Centre Nurse')]", driver, logger);
    		return CallCentreNurse;	
   	}
    public WebElement getLabResultEntryCbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement labResultEntry = Web_GeneralFunctions.findElementbyXPath("//label[contains(text(),'Lab Result Entry')]", driver, logger);
    		return labResultEntry;	
   	}
    public WebElement getMedicalCertificateCbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement medicalCertificate = Web_GeneralFunctions.findElementbyXPath("//label[contains(text(),'Medical Certificate')]", driver, logger);
    		return medicalCertificate;	
   	}
    public WebElement getCallCenterDoctorCbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement callCenterDoctor = Web_GeneralFunctions.findElementbyXPath("//label[contains(text(),'Call Center Doctor')]", driver, logger);
    		return callCenterDoctor;	
   	}
    public WebElement getSupportAdminCbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement SupportAdmin = Web_GeneralFunctions.findElementbyXPath("//label[contains(text(),'Support Admin')]", driver, logger);
    		return SupportAdmin;	
   	}
    public WebElement getSpecialityDoctorCbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement specialityDoctor = Web_GeneralFunctions.findElementbyXPath("//label[contains(text(),'Speciality Doctor')]", driver, logger);
    		return specialityDoctor;	
   	}
    //***************************************************************** Ends ******************************************************************//
    public WebElement getOperationLocationListMulBtn (WebDriver driver,ExtentTest logger) 
    
  	{
  		WebElement operationLocationList  = Web_GeneralFunctions.findElementbyXPath("//div[contains(@class,'col-xs-12')]//button[@class='multiselect dropdown-toggle btn btn-default']", driver, logger);
   		return operationLocationList;	
  	}
    // below is the options in operation location list
    public WebElement getAutomationCbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement automation = Web_GeneralFunctions.findElementbyXPath("//label[contains(text(),'Automation')]", driver, logger);
    		return automation;	
   	}
   
    //***************************************************************** Ends ******************************************************************//
 
    
    public WebElement getSaveBtn(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement save = Web_GeneralFunctions.findElementbySelector("#btnUser",driver,logger);
   			return save;	
   	}
	
     
    public WebElement getChatCbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement ChatCbx = Web_GeneralFunctions.findElementbyXPath("//label[contains(text(),'VC')]", driver, logger);
    		return ChatCbx;	
   	}
    public WebElement getVCCbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement vcCbx = Web_GeneralFunctions.findElementbyXPath("//label[contains(text(),'Chat')]", driver, logger);
    		return vcCbx;	
   	}
    // **************************************************************** update page******************************************************************//
    public WebElement getSaveBtn_updatePage(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement save = Web_GeneralFunctions.findElementbySelector("#updateUser",driver,logger);
   			return save;	
   	}
    public WebElement getForcePasswordChangeCbx(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement forcePasswordChangeCbx = Web_GeneralFunctions.findElementbyXPath("//label[contains(text(),'Yes') and @for='forcePasswordChange']", driver, logger);
    		return forcePasswordChangeCbx;	
   	}
    
	// *************************************************************** Search user page ***************************************************************************//
    
     public WebElement getSearchUserTextBox(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement searchUser = Web_GeneralFunctions.findElementbySelector("#clinicUserName",driver,logger);
   			return searchUser;	
   	}
     
     public WebElement getSearchUserBtn(WebDriver driver,ExtentTest logger) 
    	{
    		WebElement searchUserBtn = Web_GeneralFunctions.findElementbySelector("#userAdminSearch",driver,logger);
    			return searchUserBtn;	
    	}
     
     public WebElement getSearchUserIdTbx(WebDriver driver,ExtentTest logger) 
    	{
    		WebElement searchUserIdTbx = Web_GeneralFunctions.findElementbyXPath("//label[contains(text(),'Search:')]//input", driver, logger);
     		return searchUserIdTbx;	
    	}
     
    
     public WebElement getEditUserBtn(WebDriver driver,ExtentTest logger) 
 	{
 		WebElement editUserBtn = Web_GeneralFunctions.findElementbyXPath("//div[@id='pageContent']//td[5]//a[1]", driver, logger);
  		return editUserBtn;	
 	}
  
     public WebElement getActiveUserBtn(WebDriver driver,ExtentTest logger) 
  	{
  		WebElement activateBtn = Web_GeneralFunctions.findElementbyXPath("(//a[starts-with(@id,'activate')])[1]", driver, logger);
   		return activateBtn;	
  	}
     
     public WebElement getDeactivateUserBtn(WebDriver driver,ExtentTest logger) 
   	{
   		WebElement deactivateBtn = Web_GeneralFunctions.findElementbyXPath("(//a[starts-with(@id,'deactivate')])[1]", driver, logger);
    		return deactivateBtn;	
   	}
     
     public WebElement getSortDecBtn_userId(WebDriver driver,ExtentTest logger) 
    	{
    		WebElement deactivateBtn = Web_GeneralFunctions.findElementbyXPath("//th[contains(text(),'User Id')]", driver, logger);
     		return deactivateBtn;	
    	}
     
     public WebElement getfristUserIdAfterSort(WebDriver driver,ExtentTest logger) 
 	{
 		WebElement deactivateBtn = Web_GeneralFunctions.findElementbyXPath("(//td[@class='sorting_1'])[1]", driver, logger);
  		return deactivateBtn;	
 	}

}

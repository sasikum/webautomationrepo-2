package com.tatahealth.EMR.pages.MasterAdministration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;


public class MedicalCertificatesPage  {

	public WebElement getlink(WebDriver driver,ExtentTest logger) 
	{
		WebElement tablink = Web_GeneralFunctions.findElementbyXPath("(//a[contains(text(),'Medical Certificates')])[2]", driver, logger);
		return tablink;	
	}
	
	public WebElement getMedicalFitnessSelectbox(WebDriver driver,ExtentTest logger) 
	{
		WebElement selectbox = Web_GeneralFunctions.findElementbyXPath("//select[@name='certificateType']", driver, logger);
		return selectbox;	
	}
	
	public WebElement getSelectTemplateSelectbox(WebDriver driver,ExtentTest logger) 
	{
		WebElement selectbox = Web_GeneralFunctions.findElementbyXPath("//select[@name='medicalCertificateTemplate']", driver, logger);
		return selectbox;	
	}
	
	
	public WebElement getButton(WebDriver driver,ExtentTest logger) 
	{
		WebElement button = Web_GeneralFunctions.findElementbyXPath("//button[@id='toggleTemplate']", driver, logger);
		return button;	
	}
	
	public WebElement getPriviewButton(WebDriver driver,ExtentTest logger) 
	{
		WebElement button = Web_GeneralFunctions.findElementbyXPath("//button[@id='previewBtn']", driver, logger);
		return button;	
	}
	
	public WebElement getSaveButton(WebDriver driver,ExtentTest logger) 
	{
		WebElement button = Web_GeneralFunctions.findElementbyXPath("//button[@id='previewBtn']/following-sibling::button", driver, logger);
		return button;	
	}
	
	public WebElement getleftmainmenu(WebDriver driver,ExtentTest logger) 
	{
		WebElement leftmenu = Web_GeneralFunctions.findElementbyXPath("//a[@id='menu-toggle']", driver, logger);
		return leftmenu;	
	}
	
	
	public WebElement getsidelink(WebDriver driver,ExtentTest logger) 
	{
		WebElement sidelink = Web_GeneralFunctions.findElementbyXPath("//li[@name='masterAdministration']", driver, logger);
		return sidelink;	
	}
	
	public WebElement gettemplate(WebDriver driver,ExtentTest logger) 
	{
		WebElement template = Web_GeneralFunctions.findElementbyXPath("//input[@id='createTeamplateName']", driver, logger);
		return template;	
	}
	
	public WebElement getlinktexttitle(WebDriver driver,ExtentTest logger) 
	{
		WebElement linktext = Web_GeneralFunctions.findElementbyXPath("//div[@id='attributesList']//div//ul//li//a[contains(text(),'Title')]", driver, logger);
		return linktext;	
	}
	
	public WebElement getlinktextpatient(WebDriver driver,ExtentTest logger) 
	{
		WebElement linktext = Web_GeneralFunctions.findElementbyXPath("//div[@id='attributesList']//div//ul//li//a[contains(text(),'Patient')]", driver, logger);
		return linktext;	
	}
	
	public WebElement getlinktextdoctor(WebDriver driver,ExtentTest logger) 
	{
		WebElement linktext = Web_GeneralFunctions.findElementbyXPath("//div[@id='attributesList']//div//ul//li//a[contains(text(),'Doctor')]", driver, logger);
		return linktext;	
	}
	
	public WebElement getlinktextdiagnosis(WebDriver driver,ExtentTest logger) 
	{
		WebElement linktext = Web_GeneralFunctions.findElementbyXPath("//div[@id='attributesList']//div//ul//li//a[contains(text(),'Diagnosis')]", driver, logger);
		return linktext;	
	}
	
	public WebElement getlinktextnofDays(WebDriver driver,ExtentTest logger) 
	{
		WebElement linktext = Web_GeneralFunctions.findElementbyXPath("//div[@id='attributesList']//div//ul//li//a[contains(text(),'Number of days')]", driver, logger);
		return linktext;	
	}
	
	
	public WebElement getlinktextfromDate(WebDriver driver,ExtentTest logger) 
	{
		WebElement linktext = Web_GeneralFunctions.findElementbyXPath("//div[@id='attributesList']//div//ul//li//a[contains(text(),'From Date')]", driver, logger);
		return linktext;	
	}
	
	public WebElement getlinktexttoDate(WebDriver driver,ExtentTest logger) 
	{
		WebElement linktext = Web_GeneralFunctions.findElementbyXPath("//div[@id='attributesList']//div//ul//li//a[contains(text(),'To Date')]", driver, logger);
		return linktext;	
	}
	
	
	public WebElement getclosebuttonPreview(WebDriver driver,ExtentTest logger) 
	{
		WebElement btn = Web_GeneralFunctions.findElementbyXPath("//div[@id='certBody']/parent::div/following-sibling::div//button", driver, logger);
		return btn;	
	}
	
	/*Medical Certificate Page objects*/
	
	public WebElement getMedicalCertificateTab(WebDriver driver,ExtentTest logger) 
	{
		WebElement Tab = Web_GeneralFunctions.findElementbyXPath("//ul[@id='sidebarlinks']//li[@name='medicalCertificate']//a", driver, logger);
		return Tab;	
	}
	
	public WebElement getPrintmedicalCertificatelink(WebDriver driver,ExtentTest logger) 
	{
		WebElement Link = Web_GeneralFunctions.findElementbyXPath("//a[contains(text(),'Print Certificate')]", driver, logger);
		return Link;	
	}
	
	public WebElement getSearchMedicalCertificatelink(WebDriver driver,ExtentTest logger) 
	{
		WebElement Link = Web_GeneralFunctions.findElementbyXPath("//ul[@id='medicalCertificate']//li//following-sibling::li//a", driver, logger);
		return Link;	
	}
	
	public WebElement getCertificatetype_Selectbox(WebDriver driver,ExtentTest logger) 
	{
		WebElement Link = Web_GeneralFunctions.findElementbyXPath("//select[@id='certificateType']", driver, logger);
		return Link;	
	}
	
	
	public WebElement getTemplate_Selectbox(WebDriver driver,ExtentTest logger) 
	{
		WebElement Link = Web_GeneralFunctions.findElementbyXPath("//select[@name='selectTemplates']", driver, logger);
		return Link;	
	}
	
	public WebElement getDoctor_Selectbox(WebDriver driver,ExtentTest logger) 
	{
		WebElement Link = Web_GeneralFunctions.findElementbyXPath("//select[@id='doctorDropdown']", driver, logger);
		return Link;	
	}
	
	public WebElement getTitle_Selectbox(WebDriver driver,ExtentTest logger) 
	{
		WebElement Link = Web_GeneralFunctions.findElementbyXPath("//select[@id='title']", driver, logger);
		return Link;	
	}
	
	public WebElement getPatient_Inputbox(WebDriver driver,ExtentTest logger) 
	{
		WebElement Link = Web_GeneralFunctions.findElementbyXPath("//input[@id='searchpatientquick']", driver, logger);
		return Link;	
	}
	
	public WebElement getDiagonsis_Inputbox(WebDriver driver,ExtentTest logger) 
	{
		WebElement Link = Web_GeneralFunctions.findElementbyXPath("//input[@id='diagnosis']", driver, logger);
		return Link;	
	}
	
	
	public WebElement getSavePrint_Button(WebDriver driver,ExtentTest logger) 
	{
		WebElement Link = Web_GeneralFunctions.findElementbyXPath("//button[@id='savePrint']", driver, logger);
		return Link;	
	}
	
	public WebElement getNoofDays_Inputbox(WebDriver driver,ExtentTest logger) 
	{
		WebElement Link = Web_GeneralFunctions.findElementbyXPath("//input[@id='numberofdaysdisplay']", driver, logger);
		return Link;	
	}
	
	public WebElement getFromDate_Inputbox(WebDriver driver,ExtentTest logger) 
	{
		WebElement Link = Web_GeneralFunctions.findElementbyXPath("//input[@id='printfromdate']", driver, logger);
		return Link;	
	}
	
	public WebElement getToDate_Inputbox(WebDriver driver,ExtentTest logger) 
	{
		WebElement Link = Web_GeneralFunctions.findElementbyXPath("//input[@id='printtodate']", driver, logger);
		return Link;	
	}
	
	/* Search Medical Certificate page objects */
	
	
	public WebElement getPatientName_Inputbox2(WebDriver driver,ExtentTest logger) 
	{
		WebElement Link = Web_GeneralFunctions.findElementbyXPath("//input[@id='patientName']", driver, logger);
		return Link;	
	}
	
	public WebElement getMobileNo_Inputbox(WebDriver driver,ExtentTest logger) 
	{
		WebElement Link = Web_GeneralFunctions.findElementbyXPath("//input[@id='mobileNumber']", driver, logger);
		return Link;	
	}
	
	public WebElement getFromDate_Inputbox2(WebDriver driver,ExtentTest logger) 
	{
		WebElement Link = Web_GeneralFunctions.findElementbyXPath("//input[@id='textFromDate']", driver, logger);
		return Link;	
	}
	
	public WebElement getToDate_Inputbox2(WebDriver driver,ExtentTest logger) 
	{
		WebElement Link = Web_GeneralFunctions.findElementbyXPath("//input[@id='textToDate']", driver, logger);
		return Link;	
	}
	
	public WebElement getMultiCert_Selectbox(WebDriver driver,ExtentTest logger) 
	{
		WebElement Link = Web_GeneralFunctions.findElementbyXPath("//select[@id='multiCertificate']", driver, logger);
		return Link;	
	}
	
	public WebElement getSearch_Button(WebDriver driver,ExtentTest logger) 
	{
		WebElement Link = Web_GeneralFunctions.findElementbyXPath("//button[@class='btn btn-default search-medical-cert-btn']", driver, logger);
		return Link;	
	}
	
	public WebElement getUHID_Inputbox(WebDriver driver,ExtentTest logger) 
	{
		WebElement Link = Web_GeneralFunctions.findElementbyXPath("//input[@id='uhid']", driver, logger);
		return Link;	
	}
	
	public WebElement getSelectTemplate_Selectbox2(WebDriver driver,ExtentTest logger) 
	{
		WebElement selectbox = Web_GeneralFunctions.findElementbyXPath("//select[@id='selectTemplate']", driver, logger);
		return selectbox;	
	}
	
	public WebElement getPatient_selectlink(WebDriver driver,ExtentTest logger) 
	{
		WebElement selectbox = Web_GeneralFunctions.findElementbyXPath("//ul[@id='patientsearchresult']//li//div[4]//a", driver, logger);
		return selectbox;	
	}
	
	public WebElement getcertificate_selectbox(WebDriver driver,ExtentTest logger) 
	{
		WebElement selectbox = Web_GeneralFunctions.findElementbyXPath("//button[@class='multiselect dropdown-toggle btn btn-default']", driver, logger);
		return selectbox;	
	}
	
	public WebElement getcertificate_option1(WebDriver driver,ExtentTest logger) 
	{
		WebElement selectbox = Web_GeneralFunctions.findElementbyXPath("//ul[@class='multiselect-container dropdown-menu']//li[2]//a//label//input", driver, logger);
		return selectbox;	
	}
	
	public WebElement getcertificate_option2(WebDriver driver,ExtentTest logger) 
	{
		WebElement selectbox = Web_GeneralFunctions.findElementbyXPath("//ul[@class='multiselect-container dropdown-menu']//li[3]//a//label//input", driver, logger);
		return selectbox;	
	}
	
	public WebElement getdatepickerfrom(WebDriver driver,ExtentTest logger) 
	{
		WebElement selectbox = Web_GeneralFunctions.findElementbyXPath("//input[@id='printfromdate']//following-sibling::span", driver, logger);
		return selectbox;	
	}
	
	public WebElement getdatepickerto(WebDriver driver,ExtentTest logger) 
	{
		WebElement selectbox = Web_GeneralFunctions.findElementbyXPath("//input[@id='printtodate']//following-sibling::span", driver, logger);
		return selectbox;	
	}
	
	public WebElement getprint(WebDriver driver,ExtentTest logger, String patient) 
	{
		WebElement link = Web_GeneralFunctions.findElementbyXPath("//table[@id='searchMedicalCertificateResultTable']//tbody//tr//td[1][contains(text(),'"+patient+"')]//following-sibling::td[4]//a", driver, logger);
		return link;	
	}
	
	public WebElement getCurrentDate(WebDriver driver,ExtentTest logger)
	{
		WebElement currdate=Web_GeneralFunctions.findElementbySelector("div[class*='datepicker-days'] td[class*='today']", driver, logger);
		return currdate;
	}
	
	public WebElement getdoctorvalidate(WebDriver driver,ExtentTest logger, String doctor) 
	{
		WebElement link = Web_GeneralFunctions.findElementbyXPath("//div[@id='certBody'][contains(text(),'"+doctor+"')]", driver, logger);
		return link;	
	}
	
	public WebElement getpatientvalidate(WebDriver driver,ExtentTest logger, String patient) 
	{
		WebElement link = Web_GeneralFunctions.findElementbyXPath("//div[@id='certBody'][contains(text(),'"+patient+"')]", driver, logger);
		return link;	
	}
	
	
	public WebElement gettemplate_textarea(WebDriver driver,ExtentTest logger) 
	{
		WebElement textarea = Web_GeneralFunctions.findElementbyXPath("//textarea[@id='templateData']", driver, logger);
		return textarea;	
	}
	
	public WebElement gettoast_messege(WebDriver driver,ExtentTest logger) 
	{
		WebElement textarea = Web_GeneralFunctions.findElementbyXPath("//div[@id='toast-container' and @class='toast-top-right success-message']//div//div[@class='toast-message'][contains(text(),'Medical Certificate template saved successfully')]", driver, logger);
		return textarea;	
	}
	

	public WebElement geterror_message(WebDriver driver,ExtentTest logger) 
	{
		WebElement errormessage = Web_GeneralFunctions.findElementbyXPath("//div[@class='popover-content']", driver, logger);
		return errormessage;	
	}
	
	
	public WebElement getnotify_container(WebDriver driver,ExtentTest logger) 
	{
		WebElement errormessage = Web_GeneralFunctions.findElementbyXPath("//div[contains(text(),'Please select patient')]", driver, logger);
		return errormessage;	
	}
	
	public WebElement getmonth(WebDriver driver,ExtentTest logger) 
	{
		WebElement text = Web_GeneralFunctions.findElementbyXPath("(//th[@title='Select Month'])[2]", driver, logger);
		return text;	
	}
	
	public WebElement getnextbutton(WebDriver driver,ExtentTest logger) 
	{
		WebElement text = Web_GeneralFunctions.findElementbyXPath("(//th[@class='picker-switch' and @data-action='pickerSwitch' and @title='Select Month']//following-sibling::th//span)[2]", driver, logger);
		return text;	
	}
	
	
	public WebElement getday(WebDriver driver,ExtentTest logger, String day) 
	{
		WebElement text = Web_GeneralFunctions.findElementbyXPath("(//div[@class='datepicker-days']//table//tbody//td[contains(text(),'"+day+"')])[2]", driver, logger);
		return text;	
	}
	
	public WebElement getfromdatebutton(WebDriver driver,ExtentTest logger) 
	{
		WebElement text = Web_GeneralFunctions.findElementbyXPath("//input[@id='printfromdate']//following-sibling::span", driver, logger);
		return text;	
	}
	
	public WebElement gettodatebutton(WebDriver driver,ExtentTest logger) 
	{
		WebElement text = Web_GeneralFunctions.findElementbyXPath("//input[@id='printtodate']//following-sibling::span", driver, logger);
		return text;	
	}
	
	
	public WebElement getpatientsearchresult(WebDriver driver,ExtentTest logger) 
	{
		WebElement text = Web_GeneralFunctions.findElementbyXPath("//ul[@id='patientsearchresult']", driver, logger);
		return text;	
	}
	
	
	
}


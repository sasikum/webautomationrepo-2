package com.tatahealth.EMR.Scripts.AppointmentDashboard;

import java.util.ArrayList;
import org.openqa.selenium.By;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.AppointmentDashboard.AllSlotsPage;
import com.tatahealth.EMR.pages.AppointmentDashboard.AppointmentsPage;
import com.tatahealth.EMR.pages.AppointmentDashboard.ConsultationPage;
import com.tatahealth.EMR.pages.AppointmentDashboard.PreConsultationPage;
import com.tatahealth.EMR.pages.CommonPages.CommonPage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class Appointment_Doctor2 {
	
	ExtentTest logger;
	
	
	@BeforeClass(groups= {"Regression","Appointment_Doctor"})
	public static void beforeClass() throws Exception {
		Login_Doctor.LoginTest();
		System.out.println("Initialized Driver");
	}
	
	@AfterClass(groups= {"Regression","Appointment_Doctor"})
	public static void afterClass() throws Exception {
		Login_Doctor.LogoutTest();
		System.out.println("Closed Driver");
	}
	
	
	
	/***
	 * @author Bala Yaswanth
	 * @throws Exception
	 * Booked Appointment - Reschedule and Cancel
	 */
	@Test(priority=6,groups= {"Regression","Appointment_Doctor"})
	public synchronized void APP_DOC_006() throws Exception {
		
		
		logger = Reports.extent.createTest("APP_DOC_006");
		
		
		AppointmentsPage appoiPage = new AppointmentsPage();
		AllSlotsPage ASpage = new AllSlotsPage();
		
		
		//Moving to All slots Page
		Web_GeneralFunctions.click(appoiPage.getAllSlotsPage(Login_Doctor.driver, logger), "Moving to All slots page from Appointments Page", Login_Doctor.driver, logger);
		Thread.sleep(6000);
		
		//Getting Available Slot Code
		int n = ASpage.getAvailableSlot(Login_Doctor.driver, logger);
		String slotId = ASpage.getSlotId(n, Login_Doctor.driver, logger);
		String prevSlotId = Integer.toString(Integer.parseInt(slotId)-2);
		
		//Scrolling to available slot and booking appointment
		ScrollAndBookAllSlots(prevSlotId, slotId);
		
		//Going to Appointments Page
		Web_GeneralFunctions.click(ASpage.getAppointmentsPage(Login_Doctor.driver, logger), "Going to Appointments Page", Login_Doctor.driver, logger);
		Thread.sleep(6000);
		
		int appCnt = appoiPage.getAppointmentCount(Login_Doctor.driver, logger);
		Web_GeneralFunctions.scrollToElement(appoiPage.getAppointmentDropDown(appCnt, Login_Doctor.driver, logger), Login_Doctor.driver);
		
		
		Web_GeneralFunctions.click(appoiPage.getAppointmentDropDown(appCnt, Login_Doctor.driver, logger), "Clicking on Dropdown", Login_Doctor.driver, logger);
		/*generalFunctions.click(appoiPage.getReschedulefromAppointmentDropDown(appCnt, Login_Doctor.driver, logger), "Clicking on Dropdown", Login_Doctor.driver, logger);
		
		generalFunctions.click(ASpage.getTimingsfromRescheduleAlert(Login_Doctor.driver, logger), "Clicking to get timings in Reschedule Popup", Login_Doctor.driver, logger);
		generalFunctions.click(ASpage.setTimingsinRescheduleAlert((n+2), Login_Doctor.driver, logger), "Clicking to select next slot timings in Reschedule Popup", Login_Doctor.driver, logger);
		generalFunctions.click(ASpage.getOKBtnfromRescheduleAlert(Login_Doctor.driver, logger), "Clicking on Ok in Popup", Login_Doctor.driver, logger);*/
		
		Web_GeneralFunctions.click(appoiPage.getCancelfromAppointmentDropDown(appCnt, Login_Doctor.driver, logger), "Clicking on Dropdown", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ASpage.getReasondropdownfromCancelAlert(Login_Doctor.driver, logger), "Clicking to get reasons in Cancel Alert", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ASpage.setReasoninCancelAlert(Login_Doctor.driver, logger),"Clicking on reasons in Cancel Alert", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ASpage.getSubmitBtnfromCancelAlert(Login_Doctor.driver, logger), "Clicking on Submit in Alert", Login_Doctor.driver, logger);
		Thread.sleep(6000);	
	}
	
	
	
	
	
	
	/***
	 * @author Bala Yaswanth
	 * @throws Exception
	 * Booked Appointment - Consultation Option
	 */
	@Test(priority=7,groups= {"Regression","Appointment_Doctor"})
	public synchronized void APP_DOC_007() throws Exception {
		
		logger = Reports.extent.createTest("APP_DOC_007");
		
		
		AppointmentsPage appoiPage = new AppointmentsPage();
		AllSlotsPage ASpage = new AllSlotsPage();
		CommonPage CPage = new CommonPage();
		
		//Moving to All slots Page
		Web_GeneralFunctions.click(appoiPage.getAllSlotsPage(Login_Doctor.driver, logger), "Moving to All slots page from Appointments Page", Login_Doctor.driver, logger);
		Thread.sleep(6000);
		
		//Getting Available Slot Code
		int n = ASpage.getAvailableSlot(Login_Doctor.driver, logger);
		String slotId = ASpage.getSlotId(n, Login_Doctor.driver, logger);
		String prevSlotId = Integer.toString(Integer.parseInt(slotId)-2);
		
		//Scrolling to available slot and booking appointment
		ScrollAndBookAllSlots(prevSlotId, slotId);
		
		//Going to Appointments Page
		Web_GeneralFunctions.click(ASpage.getAppointmentsPage(Login_Doctor.driver, logger), "Going to Appointments Page", Login_Doctor.driver, logger);
		Thread.sleep(6000);
		
		int appCnt = appoiPage.getAppointmentCount(Login_Doctor.driver, logger);
		Web_GeneralFunctions.scrollToElement(appoiPage.getAppointmentDropDown(appCnt, Login_Doctor.driver, logger), Login_Doctor.driver);
		
		
		Web_GeneralFunctions.click(appoiPage.getAppointmentDropDown(appCnt, Login_Doctor.driver, logger), "Clicking on Dropdown", Login_Doctor.driver, logger);
		
		Web_GeneralFunctions.click(appoiPage.getConsultfromAppointmentDropDown(appCnt, Login_Doctor.driver, logger), "Clicking on Dropdown", Login_Doctor.driver, logger);
		ConsultPopupCheck();
		Thread.sleep(6000);
		
		//Preconsulting to Appointment Dashboard
		Web_GeneralFunctions.click(CPage.getPoweredByLink(Login_Doctor.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Doctor.driver, logger);
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/***
	 * @author Bala Yaswanth
	 * @throws Exception
	 * BA + Checkin - Consultation Option
	 */
	@Test(priority=8,groups= {"Regression","Appointment_Doctor"})
	public synchronized void APP_DOC_008() throws Exception {
		
		logger = Reports.extent.createTest("APP_DOC_008");
		
		
		AppointmentsPage appoiPage = new AppointmentsPage();
		AllSlotsPage ASpage = new AllSlotsPage();
		CommonPage CPage = new CommonPage();
		
		//Moving to All slots Page
		Web_GeneralFunctions.click(appoiPage.getAllSlotsPage(Login_Doctor.driver, logger), "Moving to All slots page from Appointments Page", Login_Doctor.driver, logger);
		Thread.sleep(6000);
		
		//Getting Available Slot Code
		int n = ASpage.getAvailableSlot(Login_Doctor.driver, logger);
		String slotId = ASpage.getSlotId(n, Login_Doctor.driver, logger);
		String prevSlotId = Integer.toString(Integer.parseInt(slotId)-2);
		
		//Scrolling to available slot and booking appointment
		ScrollAndBookAllSlots(prevSlotId, slotId);
		
		//Checking in booked appointment
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(n, Login_Doctor.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(n,1, Login_Doctor.driver, logger), "Clicking on Reschedule in Dropdown", Login_Doctor.driver, logger);
		Thread.sleep(6000);
		
		//Going to Appointments Page
		Web_GeneralFunctions.click(ASpage.getAppointmentsPage(Login_Doctor.driver, logger), "Going to Appointments Page", Login_Doctor.driver, logger);
		Thread.sleep(6000);
		
		int appCnt = appoiPage.getAppointmentCount(Login_Doctor.driver, logger);
		Web_GeneralFunctions.scrollToElement(appoiPage.getAppointmentDropDown(appCnt, Login_Doctor.driver, logger), Login_Doctor.driver);
		
		
		Web_GeneralFunctions.click(appoiPage.getAppointmentDropDown(appCnt, Login_Doctor.driver, logger), "Clicking on Dropdown", Login_Doctor.driver, logger);
		Thread.sleep(2000);
		Web_GeneralFunctions.click(appoiPage.getConsultfromAppointmentDropDown(appCnt, Login_Doctor.driver, logger), "Clicking on Dropdown", Login_Doctor.driver, logger);
		ConsultPopupCheck();
		Thread.sleep(6000);
		
		//Preconsulting to Appointment Dashboard
		Web_GeneralFunctions.click(CPage.getPoweredByLink(Login_Doctor.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Doctor.driver, logger);	
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/***
	 * @author Bala Yaswanth
	 * @throws Exception
	 * BA + Checkin + Pre- consulting + Pre - Consulted + Consulting + Checked Out - Warning message(Preconsulting) + Consulting(Pre consulted) + Consulting (Consulting) + View and Print(Checked Out)
	 */
	@Test(priority=9,groups= {"Regression","Appointment_Doctor"})
	public synchronized void APP_DOC_009() throws Exception {
		
		logger = Reports.extent.createTest("APP_DOC_009");
		
		
		AppointmentsPage appoiPage = new AppointmentsPage();
		AllSlotsPage ASpage = new AllSlotsPage();
		CommonPage CPage = new CommonPage();
		PreConsultationPage PreconPage = new PreConsultationPage();
		ConsultationPage ConsPage = new ConsultationPage();
		
		//Moving to All slots Page
		Web_GeneralFunctions.click(appoiPage.getAllSlotsPage(Login_Doctor.driver, logger), "Moving to All slots page from Appointments Page", Login_Doctor.driver, logger);
		Thread.sleep(6000);
		
		//Getting Available Slot Code
		int n = ASpage.getAvailableSlot(Login_Doctor.driver, logger);
		String slotId = ASpage.getSlotId(n, Login_Doctor.driver, logger);
		String prevSlotId = Integer.toString(Integer.parseInt(slotId)-2);
		
		//Scrolling to available slot and booking appointment
		ScrollAndBookAllSlots(prevSlotId, slotId);
		
		//Checking in booked appointment
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(n, Login_Doctor.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(n,1, Login_Doctor.driver, logger), "Clicking on Reschedule in Dropdown", Login_Doctor.driver, logger);
		Thread.sleep(6000);
		
		//Check-in to Preconsulting
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(n, Login_Doctor.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(n,1, Login_Doctor.driver, logger), "Clicking on Reschedule in Dropdown", Login_Doctor.driver, logger);
		Thread.sleep(6000);
		
		//Preconsulting to Appointment Dashboard
		Web_GeneralFunctions.click(CPage.getPoweredByLink(Login_Doctor.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Doctor.driver, logger);
		Thread.sleep(3000);
		
		//Preconsulting Alert Check
		int appCnt = appoiPage.getAppointmentCount(Login_Doctor.driver, logger);
		Web_GeneralFunctions.scrollToElement(appoiPage.getAppointmentDropDown(appCnt, Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.click(appoiPage.getAppointmentDropDown(appCnt, Login_Doctor.driver, logger), "Clicking on Dropdown", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(appoiPage.getConsultfromAppointmentDropDown(appCnt, Login_Doctor.driver, logger), "Clicking on Appointment in Dropdown", Login_Doctor.driver, logger);
		System.out.println(Login_Doctor.driver.findElement(By.cssSelector("#toast-container > div > div.toast-message")).getText());
		System.out.println(CPage.getToastMessage(Login_Doctor.driver, logger,10).getText());
		
		//To All slots Page
		Web_GeneralFunctions.click(appoiPage.getAllSlotsPage(Login_Doctor.driver, logger), "Moving to All slots page from Appointments Page", Login_Doctor.driver, logger);
		Thread.sleep(6000);
		
		//Preconsulting to PreConsulted
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(n, Login_Doctor.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(n,1, Login_Doctor.driver, logger), "Clicking on Reschedule in Dropdown", Login_Doctor.driver, logger);
		Thread.sleep(6000);
		
		//preconsulted to All Slots Page
		Web_GeneralFunctions.scrollToElement(PreconPage.getSaveBtn(Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.click(PreconPage.getSaveBtn(Login_Doctor.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Doctor.driver, logger);
		Thread.sleep(6000);
		
		//Moving to Appointments Page
		Web_GeneralFunctions.click(ASpage.getAppointmentsPage(Login_Doctor.driver, logger), "Moving from All slots page to Appointments Page", Login_Doctor.driver, logger);
		Thread.sleep(6000);
		
		//Appointment Consult
		Web_GeneralFunctions.click(appoiPage.getAppointmentDropDown(appCnt, Login_Doctor.driver, logger), "Clicking to get Dropdown", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(appoiPage.getConsultfromAppointmentDropDown(appCnt, Login_Doctor.driver, logger), "Clicking on Consult in Dropdown", Login_Doctor.driver, logger);
		ConsultPopupCheck();
		Thread.sleep(6000);
		
		//Consulting to Appointment Dashboard
		Web_GeneralFunctions.click(CPage.getPoweredByLink(Login_Doctor.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Doctor.driver, logger);
		
		
		//To Consult Again
		Web_GeneralFunctions.click(appoiPage.getAppointmentDropDown(appCnt, Login_Doctor.driver, logger), "Clicking on Dropdown", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(appoiPage.getConsultfromAppointmentDropDown(appCnt, Login_Doctor.driver, logger), "Clicking on Dropdown", Login_Doctor.driver, logger);
		ConsultPopupCheck();
		Thread.sleep(6000);
				
		//Scroll and Checkout
		Web_GeneralFunctions.scrollToElement(ConsPage.getCheckoutBtn(Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.click(ConsPage.getCheckoutBtn(Login_Doctor.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Doctor.driver, logger);
		NoDiagnosisCheck();
		Web_GeneralFunctions.click(ConsPage.getCloseBtninPopup(Login_Doctor.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Doctor.driver, logger);
		Thread.sleep(6000);
				
		//Billing page to Appointments
		Web_GeneralFunctions.click(CPage.getPoweredByLink(Login_Doctor.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Doctor.driver, logger);
		
		//To View Again
		Web_GeneralFunctions.scrollToElement(appoiPage.getAppointmentDropDown(appCnt, Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.click(appoiPage.getAppointmentDropDown(appCnt, Login_Doctor.driver, logger), "Clicking on Dropdown", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(appoiPage.getConsultfromAppointmentDropDown(appCnt, Login_Doctor.driver, logger), "Clicking on Dropdown", Login_Doctor.driver, logger);
		Thread.sleep(3000);
		
		//Scroll and Print
		Web_GeneralFunctions.scrollToElement(ConsPage.getPrintAllBtn(Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.click(ConsPage.getPrintAllBtn(Login_Doctor.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Doctor.driver, logger);
		Thread.sleep(3000);
		
		//Closing the printed page
		ArrayList<String> tabs = new ArrayList<String> (Login_Doctor.driver.getWindowHandles());
		Login_Doctor.driver.switchTo().window(tabs.get(1));
		Login_Doctor.driver.close();
		tabs = new ArrayList<String> (Login_Doctor.driver.getWindowHandles());
		Login_Doctor.driver.switchTo().window(tabs.get(0));
				
				
		//Moving to Appointments Page from Checked out consultation
		Web_GeneralFunctions.click(CPage.getPoweredByLink(Login_Doctor.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Doctor.driver, logger);
		Thread.sleep(10000);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	public synchronized void ScrollAndBookAllSlots(String prevSlotId,String slotId) throws Exception {
		
		
		
		AllSlotsPage ASpage = new AllSlotsPage();
		Thread.sleep(3000);
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getPlusIconforAvailableSlot(slotId, Login_Doctor.driver, logger), Login_Doctor.driver);
		Thread.sleep(3000);
		Web_GeneralFunctions.click(ASpage.getPlusIconforAvailableSlot(slotId, Login_Doctor.driver, logger), "Clicking on Plus Icon", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(ASpage.getSearchBoxinAvailableSlot(slotId, Login_Doctor.driver, logger), "8553406065", "sending value in search box", Login_Doctor.driver, logger);
		Thread.sleep(3000);
		Web_GeneralFunctions.click(ASpage.getFirstAvailableConsumer(Login_Doctor.driver, logger), "Clicking on first available consumer", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(ASpage.setReasonforVisit(Login_Doctor.driver, logger), "EMR Automation Testing", "Setting Visit Reason", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ASpage.getBookAppointmentSubmitBtn(Login_Doctor.driver, logger), "Clicking to Submit Appointment", Login_Doctor.driver, logger);
		Thread.sleep(6000);
		
	}
	
	
	
	public synchronized void appCountCheck(int prevCnt,boolean inc) throws Exception {
		
		AppointmentsPage appoiPage = new AppointmentsPage();
		int appCnt = appoiPage.getAppointmentCount(Login_Doctor.driver, logger);
		if(inc&&appCnt==prevCnt+1) {
			System.out.println("Count Properly Increased");
		}else if(!inc&&appCnt==prevCnt-1) {
			System.out.println("Count Properly Increased");
		}else {
			System.out.println("Error: Please Check Counts Again");
		}
		
		
	}
	
	
	
	public synchronized void CheinCountCheck(int prevCnt,boolean inc) throws Exception {
		
		AppointmentsPage appoiPage = new AppointmentsPage();
		int appCnt = appoiPage.getCheckedinCount(Login_Doctor.driver, logger);
		if(inc&&appCnt==prevCnt+1) {
			System.out.println("Count Properly Increased");
		}else if(!inc&&appCnt==prevCnt-1) {
			System.out.println("Count Properly Increased");
		}else {
			System.out.println("Error: Please Check Counts Again");
		}
		
		
	}
	
	
	public synchronized void CompletedCountCheck(int prevCnt,boolean inc) throws Exception {
		
		AppointmentsPage appoiPage = new AppointmentsPage();
		int appCnt = appoiPage.getCheckedinCount(Login_Doctor.driver, logger);
		if(inc&&appCnt==prevCnt+1) {
			System.out.println("Count Properly Increased");
		}else if(!inc&&appCnt==prevCnt-1) {
			System.out.println("Count Properly Increased");
		}else {
			System.out.println("Error: Please Check Counts Again");
		}
		
		
	}
	
	
	public synchronized void ConsultPopupCheck() throws Exception {
		
		
		CommonPage CPage = new CommonPage();
		try {
			Web_GeneralFunctions.click(CPage.getConsultationAlert(Login_Doctor.driver, logger), "Clicking to Checkout and start new Appointment", Login_Doctor.driver, logger);
		}catch (Exception e) {
			System.out.println("**********************Logging**********************");
			System.out.println("Unable to Checkout and Consult in Conultation Popup");
			System.out.println("**********************Logged***********************");
		}
		
	}
	
	
	public synchronized void NoDiagnosisCheck() throws Exception {
		
		
		ConsultationPage ConsPage = new ConsultationPage();
		
		try {
			Web_GeneralFunctions.click(ConsPage.getYesinSweetAlert(Login_Doctor.driver, logger),"Clicking to Checkout and start new Appointment", Login_Doctor.driver, logger);
		}catch (Exception e) {
			System.out.println("**********************Logging*******");
			System.out.println("Unable to Checkout without Diagnosis");
			System.out.println("**********************Logged********");
		}
		
	}
	
	
	
			
}
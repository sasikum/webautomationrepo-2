package com.tatahealth.EMR.Scripts.Consultation;

import static org.testng.Assert.assertTrue;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.Consultation.GlobalPrintPage;
import com.tatahealth.EMR.pages.Consultation.PrescriptionPage;
import com.tatahealth.EMR.pages.Consultation.SymptomPage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class MedicalKitTest {
	@BeforeClass(alwaysRun=true,groups= {"Regression","Appointment_Doctor"})
	public static void beforeClass() throws Exception {
		ConsultationTest currentTest = new ConsultationTest();
		Login_Doctor.LoginTest();
		currentTest.openFollowUpConsultation();	 
	}
	
	@AfterClass(groups= {"Regression","Appointment_Doctor"})
	public static void afterClass() throws Exception {
		Login_Doctor.LogoutTest();
		System.out.println("Closed Login_Doctor.driver");
	}
	public static String medicalKitName = "";
	ExtentTest logger;

	GlobalPrintPage gp = new GlobalPrintPage();
	GlobalPrintTest gpt = new GlobalPrintTest();
	SymptomTest smt = new SymptomTest();
	PrescriptionPage prescription = new PrescriptionPage();
	PrescriptionTest ptest = new PrescriptionTest();
	WebDriver driver;
	public List<String> results = new ArrayList<String>();
	String ga = "";


	 @Test(groups = { "Regression", "Login" }, priority = 831) 
	  public synchronized void clickMedicalKit() throws Exception {
	  
	  logger = Reports.extent.createTest("EMR save as medical kit");
	  gpt.moveToPrintModule();
   Web_GeneralFunctions.wait(1);
	  Web_GeneralFunctions.click(gp.getSaveAsMedicalKit(Login_Doctor.driver,logger), "Click save as medical kit", Login_Doctor.driver, logger);
	  Web_GeneralFunctions.wait(1);
	  }
	  
	  
	  
	  @Test(groups= {"Regression","Login"},priority=832) 
	  public synchronized void saveMedicalKitWithoutName()throws Exception{
	  
	  logger = Reports.extent.createTest("EMR save  medical kit without kit name");
	  gpt.saveMedicalKit(); 
	  Web_GeneralFunctions.wait(1);
	  String message =  Web_GeneralFunctions.getText(gp.getMessage(Login_Doctor.driver, logger),"Get warning message", Login_Doctor.driver, logger); 
	  System.out.println("message : "+message);
	  if(message.equalsIgnoreCase("Please enter medical kit name")) {
	  assertTrue(true); }
	  else
	  { assertTrue(false);
	  
	  }
	  gpt.closeMedicalKitButton(); 
	  }
	  
	  
	  
	  
	  @Test(groups= {"Regression","Login"},priority=833) public synchronized void
	  saveMedicalKitWithProperValue()throws Exception{
	  
	  logger = Reports.extent.createTest("EMR  medical kit with proper values");
	  SymptomPage symptom = new SymptomPage();
	String duration = "5day$";
	String otherDetails = "Vomit**";
	  smt.moveToSymtpomModule(); 
	  	smt.Consultation_001();
	  // filling duration and other details
		
		/*
		 * Web_GeneralFunctions.sendkeys(symptom.getDuration(Login_Doctor.driver,1,
		 * logger),duration,"Sending duration value to text box", Login_Doctor.driver,
		 * logger); Thread.sleep(300);
		 * Web_GeneralFunctions.sendkeys(symptom.getOtherDetails(Login_Doctor.driver,1,
		 * logger), otherDetails, "Sending duration value to text box",
		 * Login_Doctor.driver, logger);
		 */
		 		
	 	medicalKitName = RandomStringUtils.randomAlphabetic(10); 
	 	gpt.moveToPrintModule();
	 	 Web_GeneralFunctions.wait(2);
		 clickMedicalKit();
	  Web_GeneralFunctions.sendkeys(gp.getMedicalKitName(Login_Doctor.driver,logger), medicalKitName, "Sending medical kit value", Login_Doctor.driver, logger);
	  gpt.saveMedicalKit(); 
	  Web_GeneralFunctions.wait(1);
	  String message =Web_GeneralFunctions.getText(gp.getMessage(Login_Doctor.driver, logger),"Get success message", Login_Doctor.driver, logger);
	
	  if(message.equalsIgnoreCase("Medical kit created successfully.")) {
		  assertTrue(true); 
		  }
	  else { 
		  	Web_GeneralFunctions.wait(3);
			message =Web_GeneralFunctions.getText(gp.getMessage(Login_Doctor.driver, logger),"Get success message", Login_Doctor.driver, logger);
			System.out.println("Message" +message);
		//Adding extra condition since case sheet updated message is appearing.
			if(message.equalsIgnoreCase("Medical kit created successfully.")) {
				  assertTrue(true); 
				  }
			  else {
				  assertTrue(false);
			  }
		  }

	 
	  }
	  
	  @Test(groups= {"Regression","Login"},priority=834) 
	  public synchronized void saveMedicalKitWithDuplicateName()throws Exception{
	  
	  logger = Reports.extent.createTest("EMR  medical kit with duplicate kit name");
	  clickMedicalKit();
	  Web_GeneralFunctions.clear(gp.getMedicalKitName(Login_Doctor.driver,logger), "Clearing the value", driver, logger);
	  Web_GeneralFunctions.sendkeys(gp.getMedicalKitName(Login_Doctor.driver, logger), medicalKitName, "sending duplicate value", Login_Doctor.driver,logger);
	  Web_GeneralFunctions.wait(2);
	  gpt.saveMedicalKit();
	  String text = Web_GeneralFunctions.getText(gp.getMessage(Login_Doctor.driver, logger), "Get duplicate alert", Login_Doctor.driver, logger);
	  Web_GeneralFunctions.wait(2);
	  if(text.equalsIgnoreCase("Medical kit name already exists.")) {
		  assertTrue(true);
		  }
	  else
	  { 
		  assertTrue(false); 
	  } 
	  Web_GeneralFunctions.wait(2);
	  gpt.closeMedicalKitButton(); 
	  Web_GeneralFunctions.wait(2); }
	  
	 @Test(groups= {"Regression","Login"},priority=835) 
	  public synchronized void savedMedicalKitPresentInMedicalKitModule()throws Exception{
	  PrescriptionPage prescription = new PrescriptionPage();

	  
	  logger = Reports.extent.createTest("EMR saved medical kit present in medical kit module");
	  Web_GeneralFunctions.click(gp.moveToSymptomModule(Login_Doctor.driver,logger), "move to symptom module", Login_Doctor.driver, logger);
	  Web_GeneralFunctions.wait(1);
	  String text = Web_GeneralFunctions.getText(gp.getMedicalKitFromDropDown(medicalKitName,Login_Doctor.driver, logger), "Get medical kit name", Login_Doctor.driver,logger); 
	  Web_GeneralFunctions.wait(1);
	  if(text.equalsIgnoreCase(medicalKitName)) {
		  
	  assertTrue(true); }else { assertTrue(false); 
	  } Web_GeneralFunctions.wait(1);
	  //duplicatecheck ...........................................
	  gp.getMedicalKitSearch( Login_Doctor.driver, logger).clear();
	  Web_GeneralFunctions.wait(1);
	  Web_GeneralFunctions.click(gp.getMedicalKitFromDropDown(medicalKitName,Login_Doctor.driver, logger), "Get medical kit name", Login_Doctor.driver,logger);
	  Web_GeneralFunctions.wait(6);
	  Web_GeneralFunctions.click(gp.getAppyMedicalKitButton(Login_Doctor.driver,logger), "Click on appy kit button", Login_Doctor.driver, logger);
	  
		
		Robot robot = new Robot();
		Web_GeneralFunctions.wait(1);
			robot.keyPress(KeyEvent.VK_TAB);
			robot.keyRelease(KeyEvent.VK_TAB);
			robot.keyPress(KeyEvent.VK_TAB);
			robot.keyRelease(KeyEvent.VK_TAB);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
	  
			Web_GeneralFunctions.wait(1);
		
		Web_GeneralFunctions.click(prescription.getNOButtonInDuplicateBox(Login_Doctor.driver, logger), "click No button in duplicate medicine pop up", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
	 }
	 
	 
	 @Test(groups = { "Regression", "Login" }, priority = 836)
		public synchronized void setAsDefaultWithAllDivs() throws Exception {
		 gpt.moveToPrintModule();
		Web_GeneralFunctions.wait(2);
		
			/*
			 * gp.consultationSectionsCaseSheet(Login_Doctor.driver,logger).click();
			 * Thread.sleep(500);
			 * gp.consultationSectionsGA(Login_Doctor.driver,logger).click();
			 * gp.consultationSectionsVitals(Login_Doctor.driver,logger).click();
			 * Thread.sleep(500); gp.consultationSectionsDiagnosis(Login_Doctor.driver,
			 * logger).click(); Thread.sleep(500);
			 * gp.consultationSectionsFollowUp(Login_Doctor.driver,logger).click();
			 * Thread.sleep(500); gp.consultationSectionsLabTests(Login_Doctor.driver,
			 * logger).click(); Thread.sleep(500);
			 * gp.consultationSectionsMedicalHistory(Login_Doctor.driver,logger).click();
			 * Thread.sleep(500); gp.consultationSectionsReferral(Login_Doctor.driver,
			 * logger).click(); Thread.sleep(500);
			 * gp.consultationSectionsSymptoms(Login_Doctor.driver,logger).click();
			 * Thread.sleep(500); gp.consultationSectionsPrescription(Login_Doctor.driver,
			 * logger).click(); Thread.sleep(500);
			 */
		
			gpt.setAsDefault();
		  Web_GeneralFunctions.wait(1);

		}

	    @Test(groups = { "Regression", "Login" }, priority = 837)
	     public synchronized void checkoutPrint() throws Exception {
		logger = Reports.extent.createTest("EMR fill sysmtoms");
		try {
			gpt.moveToPrintModule();
			gpt.checkout();
			gpt.clickViewSummary();
			//readCheckOutPdf();
		} catch (Exception e) {
			 
		}
		Thread.sleep(1000);

	}

	
}

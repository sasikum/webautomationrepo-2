package com.tatahealth.EMR.Scripts.PatientAdvanceSearch;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.tatahealth.EMR.pages.PatientAdvanceSearch.AdvancePatientSearch;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;
import com.tatahealth.ReusableModules.Web_Testbase;
import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.API.libraries.SheetsAPI;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.Scripts.Login.PWALoginTest;

public class PatientAdvanceSearch3 {

	public static ExtentTest logger;
	AdvancePatientSearch adPatSearch = new AdvancePatientSearch();

	private static String specificRole;
	private static String input;
	private static final String NEWCALCENTREDOC="NewCalCentre";
	
	@BeforeClass(groups= {"Regression","PatientAdvanceSearch","Sanity"})
	@Parameters("role")
	public static void beforeClass(String role) throws Exception {
		Login_Doctor.executionName="Patient Advance Search";
		PWALoginTest.LoginUser();
		PWALoginTest.LogoutUser();
		Login_Doctor.LoginTestwithDiffrentUser(role);
		specificRole=role;
		AdvancePatientSearch.getAdvanceSearchBtn(Login_Doctor.driver, logger).click();

	}

	@AfterClass(groups= {"Regression","PatientAdvanceSearch","Sanity"})
	public static void afterClass() {
		Login_Doctor.LogoutTest();
		Reports.extent.flush();
	}
	
	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch45() throws Exception {
			logger = Reports.extent.createTest("Verifying the length of alpha for Address Field");
			input=RandomStringUtils.randomAlphabetic(60);
			Web_GeneralFunctions.sendkeys(adPatSearch.getAddressText(Login_Doctor.driver, logger), input,"Entering Alphabets", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(3);
			Assert.assertTrue(adPatSearch.isValueEntered(adPatSearch.getAddressText(Login_Doctor.driver, logger),input));
			adPatSearch.getAddressText(Login_Doctor.driver, logger).clear();
		}
	
	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch46() throws Exception {
			logger = Reports.extent.createTest("Verifying the length of numeric values for Address Field");
			input=RandomStringUtils.randomNumeric(60);
			Web_GeneralFunctions.sendkeys(adPatSearch.getAddressText(Login_Doctor.driver, logger), input,"Entering Alphabets", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(3);
			Assert.assertTrue(adPatSearch.isValueEntered(adPatSearch.getAddressText(Login_Doctor.driver, logger),input));
			adPatSearch.getAddressText(Login_Doctor.driver, logger).clear();
		}
	
	@Test(dataProvider = "Patient search data",groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch47(String data) throws Exception {
		logger = Reports.extent.createTest("Verifying if the  special characters can be entered for Address Field");
		adPatSearch.getAddressText(Login_Doctor.driver, logger).clear();
		Web_GeneralFunctions.sendkeys(adPatSearch.getAddressText(Login_Doctor.driver, logger), data,"Entering special characters" + data, Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Assert.assertTrue(adPatSearch.isValueEntered(adPatSearch.getAddressText(Login_Doctor.driver, logger),data));
		adPatSearch.getAddressText(Login_Doctor.driver, logger).clear();
	}
	
	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch48() throws Exception {
		logger = Reports.extent.createTest("Verifying if the  special characters can be entered for Address Field");
		adPatSearch.getAddressText(Login_Doctor.driver, logger).clear();
		input=SheetsAPI.getDataProperties(Web_Testbase.input + ".Address");
		Web_GeneralFunctions.sendkeys(adPatSearch.getAddressText(Login_Doctor.driver, logger), input,"Entering valid address" + input, Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(adPatSearch.getSearchBtn(Login_Doctor.driver, logger), "Clicking on Search button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(adPatSearch.getViewDetailsBtn(Login_Doctor.driver, logger), "Clicking on view details", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(adPatSearch.getMoreOptions(Login_Doctor.driver, logger), "Clicking on more options", Login_Doctor.driver, logger);
		String patientadd1=adPatSearch.getAddressLineFromViewDetails(Login_Doctor.driver,logger).getAttribute("value");
		Assert.assertTrue(patientadd1.equals(input), "Verifying the address with search result");
		Web_GeneralFunctions.click(adPatSearch.getEmrMenu(Login_Doctor.driver, logger), "Clicking on Main menu", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(adPatSearch.getModuleFromLeftMenu(Login_Doctor.driver, logger,"Appointments"), "Clicking on Appointments", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(AdvancePatientSearch.getAdvanceSearchBtn(Login_Doctor.driver, logger), "Clicking to go back to advance search page", Login_Doctor.driver, logger);
		}
	
	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch49() throws Exception {
			logger = Reports.extent.createTest("Verifying the No result found for Address Field");
			input= RandomStringUtils.randomAlphanumeric(60);
			Web_GeneralFunctions.sendkeys(adPatSearch.getAddressText(Login_Doctor.driver, logger), input,"No result found ", Login_Doctor.driver, logger);
			validateNoResultErrorMessage();
			adPatSearch.getAddressText(Login_Doctor.driver, logger).clear();
		}

	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch50() throws Exception {
			logger = Reports.extent.createTest("Verifying the length of alpha numeric values in Address field");
			input=RandomStringUtils.randomAlphanumeric(70);
			Web_GeneralFunctions.sendkeys(adPatSearch.getAddressText(Login_Doctor.driver, logger), input,"Entering Alphabets", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(3);
			int alphacount = adPatSearch.getAddressText(Login_Doctor.driver, logger).getAttribute("value").length();
			Assert.assertTrue(alphacount == 60, "Limit of alphanumeric characters added in Address is 60");
			adPatSearch.getAddressText(Login_Doctor.driver, logger).clear();
		}
	
	
	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch51()throws Exception{
		logger=Reports.extent.createTest("Verifying the popup and date selection for Visit From date");
		Web_GeneralFunctions.click(adPatSearch.getStartDat(Login_Doctor.driver, logger), "Clicking on start date textbox",Login_Doctor.driver, logger);
		adPatSearch.getCalendarPopup(Login_Doctor.driver, logger).isDisplayed();
		input=SheetsAPI.getDataProperties(Web_Testbase.input+".FutureDate");
		adPatSearch.selectDate(input,Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		String pastDate=adPatSearch.getStartDat(Login_Doctor.driver,logger).getAttribute("value");
		Assert.assertTrue(pastDate.equals(adPatSearch.getFormattedDate(input)),"Date,Month and Year Selected");
		Web_GeneralFunctions.click(AdvancePatientSearch.getAdvanceSearchBtn(Login_Doctor.driver, logger), "Clicking to go back to advance search page", Login_Doctor.driver, logger);		 		
		Web_GeneralFunctions.wait(3);
	}
	
	//Disabling this test case because intermittently current date is getting appended
	@Test(enabled=false,groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch52() throws Exception {
		logger = Reports.extent.createTest("Verifying the Startdate field with characters");
		Web_GeneralFunctions.click(adPatSearch.getStartDat(Login_Doctor.driver, logger),"Clicking on Start date Field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(adPatSearch.getCurrentDate(Login_Doctor.driver, logger),"Clicking on current date from date picker", Login_Doctor.driver, logger);
		Web_GeneralFunctions.clear(adPatSearch.getStartDat(Login_Doctor.driver, logger), "Clearing the Start date Field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		input= RandomStringUtils.randomAlphabetic(10);
		
		Web_GeneralFunctions.sendkeys(adPatSearch.getStartDat(Login_Doctor.driver, logger), input,"Entering Alphabets ", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Assert.assertTrue(adPatSearch.isValueEntered(adPatSearch.getStartDat(Login_Doctor.driver, logger),input),"Verifying if the characters are entered");

		Web_GeneralFunctions.click(adPatSearch.getSearchBtn(Login_Doctor.driver, logger), "Clicking on Search Button",Login_Doctor.driver, logger);
		String enteronefield = adPatSearch.getToastMessage(Login_Doctor.driver, logger).getText();
		Assert.assertTrue("Please enter atleast one field".equals(enteronefield.trim()),"Actual Toast message with expected");
		Assert.assertFalse(adPatSearch.isValueEntered(adPatSearch.getStartDat(Login_Doctor.driver, logger),input),"Verifying the empty date field");
	}
	
	//Disabling this test case because intermittently current date is getting appended
	@Test(enabled=false,groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch53() throws Exception {
		logger = Reports.extent.createTest("Verifying the Startdate field with numeric");
		Web_GeneralFunctions.click(adPatSearch.getStartDat(Login_Doctor.driver, logger),"Clicking on Start date Field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(adPatSearch.getCurrentDate(Login_Doctor.driver, logger),"Clicking on current date from date picker", Login_Doctor.driver, logger);
		Web_GeneralFunctions.clear(adPatSearch.getStartDat(Login_Doctor.driver, logger), "Clearing the Start date", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		
		input= RandomStringUtils.randomNumeric(10);
		Web_GeneralFunctions.sendkeys(adPatSearch.getStartDat(Login_Doctor.driver, logger), input,"Entering numbers", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Assert.assertTrue(adPatSearch.isValueEntered(adPatSearch.getStartDat(Login_Doctor.driver, logger),input),"Verifying if the numeric are entered");

		Web_GeneralFunctions.click(adPatSearch.getSearchBtn(Login_Doctor.driver, logger), "Clicking on Search Button",Login_Doctor.driver, logger);
		String enteronefield = adPatSearch.getToastMessage(Login_Doctor.driver, logger).getText();
		Assert.assertTrue("Please enter atleast one field".equals(enteronefield.trim()),"Actual Toast message with expected");
		Assert.assertFalse(adPatSearch.isValueEntered(adPatSearch.getStartDat(Login_Doctor.driver, logger),input),"Verifying the empty date field");
	}
	
	//Disabling this test case because intermittently current date is getting appended
	@Test(enabled=false,groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch54() throws Exception {
		logger = Reports.extent.createTest("Verifying the Startdate field with special characters");
		Web_GeneralFunctions.click(adPatSearch.getStartDat(Login_Doctor.driver, logger),"Clicking on Start date", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(adPatSearch.getCurrentDate(Login_Doctor.driver, logger),"Clicking on current date from date picker", Login_Doctor.driver, logger);
		Web_GeneralFunctions.clear(adPatSearch.getStartDat(Login_Doctor.driver, logger), "Clearing the Start date", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		
		input= SheetsAPI.getDataProperties(Web_Testbase.input + ".SpecialChars");
		Web_GeneralFunctions.sendkeys(adPatSearch.getStartDat(Login_Doctor.driver, logger), input,"Entering special characters ", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Assert.assertTrue(adPatSearch.isValueEntered(adPatSearch.getStartDat(Login_Doctor.driver, logger),input),"Verifying if the special characters are entered");

		Web_GeneralFunctions.click(adPatSearch.getSearchBtn(Login_Doctor.driver, logger), "Clicking on Search Button",Login_Doctor.driver, logger);
		String enteronefield = adPatSearch.getToastMessage(Login_Doctor.driver, logger).getText();
		Assert.assertTrue("Please enter atleast one field".equals(enteronefield.trim()),"Actual Toast message with expected");
		Assert.assertFalse(adPatSearch.isValueEntered(adPatSearch.getStartDat(Login_Doctor.driver, logger),input),"Verifying the empty date field");
	}
	
	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch55() throws Exception {
		logger = Reports.extent.createTest("Verifying the result with Visit From date alone");
		Web_GeneralFunctions.click(adPatSearch.getStartDat(Login_Doctor.driver, logger),"Clicking on Start date", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(adPatSearch.getCurrentDate(Login_Doctor.driver, logger),"Clicking on current date from date picker", Login_Doctor.driver, logger);
	    
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(adPatSearch.getSearchBtn(Login_Doctor.driver, logger), "Clicking on Search Button",Login_Doctor.driver, logger);
		String entertodate = adPatSearch.getVisitFromToastMessage(Login_Doctor.driver, logger).getText();
		Assert.assertTrue("Please enter Visit To Date".equals(entertodate.trim()),"Actual Toast message with expected");	
		adPatSearch.getStartDat(Login_Doctor.driver, logger).clear();
	}
	
	
	//This test case covers both advanceSearch56 and advanceSearch63
	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch56() throws Exception {
		logger = Reports.extent.createTest("Verifying the No Result found details when both visit from date and to date are entered");
		Web_GeneralFunctions.clear(adPatSearch.getStartDat(Login_Doctor.driver, logger), "clearing the startdate", Login_Doctor.driver, logger);
		Web_GeneralFunctions.clear(adPatSearch.getEndDat(Login_Doctor.driver, logger), "clearing the end date", Login_Doctor.driver, logger);
		
		Web_GeneralFunctions.click(adPatSearch.getStartDat(Login_Doctor.driver, logger),"Clicking on start date textbox", Login_Doctor.driver, logger);
		input=SheetsAPI.getDataProperties(Web_Testbase.input+".NoResDate");
		Web_GeneralFunctions.sendkeys(adPatSearch.getStartDat(Login_Doctor.driver, logger), input, "Sending current date for visit from date", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(adPatSearch.getEndDat(Login_Doctor.driver, logger), input, "Sending current date for visit to date", Login_Doctor.driver, logger);
		validateNoResultErrorMessage();
		adPatSearch.getStartDat(Login_Doctor.driver, logger).clear();
		adPatSearch.getEndDat(Login_Doctor.driver, logger).clear();
		Web_GeneralFunctions.click(AdvancePatientSearch.getAdvanceSearchBtn(Login_Doctor.driver, logger),"Clicking on Advance search to reset", Login_Doctor.driver, logger);
			
	}
	
	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch57() throws Exception {
		logger = Reports.extent.createTest("Verifying the selection of current past and future date");
		 Web_GeneralFunctions.wait(3); 
		 Web_GeneralFunctions.click(adPatSearch.getStartDat(Login_Doctor.driver, logger),"Clicking on start date textbox", Login_Doctor.driver, logger);
		 
		 //Current date
		 Web_GeneralFunctions.click(adPatSearch.getCurrentDate(Login_Doctor.driver, logger),"Clicking on current date from date picker", Login_Doctor.driver, logger);
	     String currday = adPatSearch.getStartDat(Login_Doctor.driver, logger).getAttribute("value");
		 Assert.assertTrue(currday.equals(getCurrSysDate()), "Verifying if the current date is selected");
		 Web_GeneralFunctions.wait(3); 
		 adPatSearch.getStartDat(Login_Doctor.driver, logger).clear();
		 
		 //Pastdate
		 Web_GeneralFunctions.click(adPatSearch.getStartDat(Login_Doctor.driver, logger),"Clicking on start date Field", Login_Doctor.driver, logger);
		 input=SheetsAPI.getDataProperties(Web_Testbase.input+".PastDate");
			
		 adPatSearch.selectDate(input,Login_Doctor.driver, logger);
		 Web_GeneralFunctions.wait(3); 
		 String pastDate=adPatSearch.getStartDat(Login_Doctor.driver,logger).getAttribute("value");
		 Assert.assertTrue(pastDate.equals(adPatSearch.getFormattedDate(input)),"Past Date,Month and Year Selected");
		 adPatSearch.getStartDat(Login_Doctor.driver, logger).clear();
		 
		 //Future date
		 Web_GeneralFunctions.click(adPatSearch.getStartDat(Login_Doctor.driver, logger),"Clicking on start date Field", Login_Doctor.driver, logger);
		 input=SheetsAPI.getDataProperties(Web_Testbase.input+".FutureDate");
		 adPatSearch.selectDate(input,Login_Doctor.driver, logger);
		 Web_GeneralFunctions.wait(3); 
		 String futdate=adPatSearch.getStartDat(Login_Doctor.driver,logger).getAttribute("value");
		 Assert.assertTrue(futdate.equals(adPatSearch.getFormattedDate(input)),"Future Date,Month and Year Selected");
		 adPatSearch.getStartDat(Login_Doctor.driver, logger).clear();
	}
	
	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch58()throws Exception{
		logger=Reports.extent.createTest("Verifying the popup and date selection for Visit To date");
		Web_GeneralFunctions.click(adPatSearch.getEndDat(Login_Doctor.driver, logger), "Clicking on end date textbox",Login_Doctor.driver, logger);
		adPatSearch.getCalendarPopup(Login_Doctor.driver, logger).isDisplayed();
		input=SheetsAPI.getDataProperties(Web_Testbase.input+".FutureDate");
		adPatSearch.selectDate(input,Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		String pastDate=adPatSearch.getEndDat(Login_Doctor.driver,logger).getAttribute("value");
		Assert.assertTrue(pastDate.equals(adPatSearch.getFormattedDate(input)),"Date,Month and Year Selected");
		Web_GeneralFunctions.click(AdvancePatientSearch.getAdvanceSearchBtn(Login_Doctor.driver, logger), "Clicking to go back to advance search page", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
	}
	
	
	//Disabling this test case because intermittently current date is getting appended
	@Test(enabled=false,groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch59() throws Exception {
		logger = Reports.extent.createTest("Verifying the End Date field with characters");
		Web_GeneralFunctions.click(adPatSearch.getEndDat(Login_Doctor.driver, logger),"Clicking on end date textbox", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(adPatSearch.getCurrentDate(Login_Doctor.driver, logger),"Clicking on current date from date picker", Login_Doctor.driver, logger);
		Web_GeneralFunctions.clear(adPatSearch.getEndDat(Login_Doctor.driver, logger), "Clearing end date field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		input = RandomStringUtils.randomAlphabetic(10);
		Web_GeneralFunctions.sendkeys(adPatSearch.getEndDat(Login_Doctor.driver, logger), input,"Entering Alphabets ", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Assert.assertTrue(adPatSearch.isValueEntered(adPatSearch.getEndDat(Login_Doctor.driver, logger),input),"Verifying if the characters are entered");
		Web_GeneralFunctions.click(adPatSearch.getSearchBtn(Login_Doctor.driver, logger), "Clicking on Search Button",Login_Doctor.driver, logger);
		
		String enteronefield = adPatSearch.getToastMessage(Login_Doctor.driver, logger).getText();
		Assert.assertTrue("Please enter atleast one field".equals(enteronefield.trim()),"Actual Toast message with expected");
		Assert.assertFalse(adPatSearch.isValueEntered(adPatSearch.getEndDat(Login_Doctor.driver, logger),input),"Verifying the empty date field");
	}
	
	//Disabling this test case because intermittently current date is getting appended
	@Test(enabled=false,groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch60() throws Exception {
		
		logger = Reports.extent.createTest("Verifying the End Date field with numeric");
		Web_GeneralFunctions.click(adPatSearch.getEndDat(Login_Doctor.driver, logger),"Clicking on end date textbox", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(adPatSearch.getCurrentDate(Login_Doctor.driver, logger),"Clicking on current date from date picker", Login_Doctor.driver, logger);
		Web_GeneralFunctions.clear(adPatSearch.getEndDat(Login_Doctor.driver, logger), "Clearing end date field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		input= RandomStringUtils.randomNumeric(10);
		Web_GeneralFunctions.sendkeys(adPatSearch.getEndDat(Login_Doctor.driver, logger), input,"Entering numbers", Login_Doctor.driver, logger);
		
		Assert.assertTrue(adPatSearch.isValueEntered(adPatSearch.getEndDat(Login_Doctor.driver, logger),input),"Verifying if the numeric are entered");

		Web_GeneralFunctions.click(adPatSearch.getSearchBtn(Login_Doctor.driver, logger), "Clicking on Search Button",Login_Doctor.driver, logger);
		String enteronefield = adPatSearch.getToastMessage(Login_Doctor.driver, logger).getText();
		Assert.assertTrue("Please enter atleast one field".equals(enteronefield.trim()),"Actual Toast message with expected");
		Assert.assertFalse(adPatSearch.isValueEntered(adPatSearch.getEndDat(Login_Doctor.driver, logger),input),"Verifying the empty date field");
	}
	
	//Disabling this test case because intermittently current date is getting appended
	@Test(enabled=false,groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch61() throws Exception {
		logger = Reports.extent.createTest("Verifying the End Date field with special characters");
		Web_GeneralFunctions.click(adPatSearch.getEndDat(Login_Doctor.driver, logger),"Clicking on end date textbox", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(adPatSearch.getCurrentDate(Login_Doctor.driver, logger),"Clicking on current date from date picker", Login_Doctor.driver, logger);

		Web_GeneralFunctions.clear(adPatSearch.getEndDat(Login_Doctor.driver, logger), "Clearing end date field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		input= SheetsAPI.getDataProperties(Web_Testbase.input+".SpecialChars");
		Web_GeneralFunctions.sendkeys(adPatSearch.getEndDat(Login_Doctor.driver, logger), input,"Entering special characters ", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Assert.assertTrue(adPatSearch.isValueEntered(adPatSearch.getEndDat(Login_Doctor.driver, logger),input),"Verifying if the special characters are entered");

		Web_GeneralFunctions.click(adPatSearch.getSearchBtn(Login_Doctor.driver, logger), "Clicking on Search Button",Login_Doctor.driver, logger);
		String enteronefield = adPatSearch.getToastMessage(Login_Doctor.driver, logger).getText();
		Assert.assertTrue("Please enter atleast one field".equals(enteronefield.trim()),"Actual Toast message with expected");
		Assert.assertFalse(adPatSearch.isValueEntered(adPatSearch.getEndDat(Login_Doctor.driver, logger),input),"Verifying the empty date field");
	}
	
	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch62() throws Exception {
		logger = Reports.extent.createTest("Verifying the result with Visit From date alone");
		Web_GeneralFunctions.clear(adPatSearch.getEndDat(Login_Doctor.driver, logger), "clearing the end date", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(adPatSearch.getEndDat(Login_Doctor.driver, logger),"Clicking on end date textbox", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(adPatSearch.getCurrentDate(Login_Doctor.driver, logger),"Clicking on current date from date picker", Login_Doctor.driver, logger);
	    
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(adPatSearch.getSearchBtn(Login_Doctor.driver, logger), "Clicking on Search Button",Login_Doctor.driver, logger);
		String entertodate = adPatSearch.getVisitFromToastMessage(Login_Doctor.driver, logger).getText();
		Assert.assertTrue("Please enter Visit From Date".equals(entertodate.trim()),"Actual Toast message with expected");		
		adPatSearch.getEndDat(Login_Doctor.driver, logger).clear();
	}
	
	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch64() throws Exception {
		logger = Reports.extent.createTest("Verifying the selection of current past and future date");
		  Web_GeneralFunctions.wait(3); 
		 Web_GeneralFunctions.click(adPatSearch.getEndDat(Login_Doctor.driver, logger),"Clicking on end date textbox", Login_Doctor.driver, logger);
		 
		 //Current date
		 Web_GeneralFunctions.click(adPatSearch.getCurrentDate(Login_Doctor.driver, logger),"Clicking on current date from date picker", Login_Doctor.driver, logger);
	     String currday = adPatSearch.getEndDat(Login_Doctor.driver, logger).getAttribute("value");
		 Assert.assertTrue(currday.equals(getCurrSysDate()), "Verifying if the current date is selected");
		 Web_GeneralFunctions.wait(3); 
		 adPatSearch.getEndDat(Login_Doctor.driver, logger).clear();
		 
		 //Pastdate
		 Web_GeneralFunctions.click(adPatSearch.getEndDat(Login_Doctor.driver, logger),"Clicking on end date Field", Login_Doctor.driver, logger);
		 String date=SheetsAPI.getDataProperties(Web_Testbase.input+".PastDate");
		 adPatSearch.selectDate(date,Login_Doctor.driver, logger);
		 Web_GeneralFunctions.wait(3); 
		 String pastDate=adPatSearch.getEndDat(Login_Doctor.driver,logger).getAttribute("value");
		 Assert.assertTrue(pastDate.equals(adPatSearch.getFormattedDate(date)),"Past Date,Month and Year Selected");
		 adPatSearch.getEndDat(Login_Doctor.driver, logger).clear();
		 
		 //Future date
		 Web_GeneralFunctions.click(adPatSearch.getEndDat(Login_Doctor.driver, logger),"Clicking on end date Field", Login_Doctor.driver, logger);
		 date=SheetsAPI.getDataProperties(Web_Testbase.input+".FutureDate");
		 
		 adPatSearch.selectDate(date,Login_Doctor.driver, logger);
		 Web_GeneralFunctions.wait(3); 
		 String futdate=adPatSearch.getEndDat(Login_Doctor.driver,logger).getAttribute("value");
		 Assert.assertTrue(futdate.equals(adPatSearch.getFormattedDate(date)),"Future Date,Month and Year Selected");
		 adPatSearch.getEndDat(Login_Doctor.driver, logger).clear();
	}
	
	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch65() throws Exception {
		logger = Reports.extent.createTest("Verifying the No Result found details when both visit from date and to date are entered");
		adPatSearch.getStartDat(Login_Doctor.driver, logger).clear();
		Web_GeneralFunctions.click(AdvancePatientSearch.getAdvanceSearchBtn(Login_Doctor.driver, logger),"Clicking on Advance search to reset", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(adPatSearch.getStartDat(Login_Doctor.driver, logger),"Clicking on Start date Field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(adPatSearch.getFutureDate(Login_Doctor.driver, logger),"Clicking on current date from date picker", Login_Doctor.driver, logger);
		 
		 Web_GeneralFunctions.click(adPatSearch.getEndDat(Login_Doctor.driver, logger),"Clicking on end date Field", Login_Doctor.driver, logger);
		 Web_GeneralFunctions.click(adPatSearch.getCurrentDate(Login_Doctor.driver, logger),"Clicking on current date from date picker", Login_Doctor.driver, logger);
		 Web_GeneralFunctions.wait(3);
		
		 Web_GeneralFunctions.click(adPatSearch.getSearchBtn(Login_Doctor.driver, logger), "Clicking on Search Button",Login_Doctor.driver, logger);
		 String warngmsg = adPatSearch.getToastMessage(Login_Doctor.driver, logger).getText();
		 Assert.assertTrue("Visit To Date should be greater than Visit From Date".equals(warngmsg.trim()),"Actual Toast message with expected");
		
		 adPatSearch.getStartDat(Login_Doctor.driver, logger).clear();
		 adPatSearch.getEndDat(Login_Doctor.driver, logger).clear();
	}
	
	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch66() throws Exception {
		logger = Reports.extent.createTest("Verifying if characters are entered in Govt ID");
		adPatSearch.getGovtID(Login_Doctor.driver, logger).clear();
		input = RandomStringUtils.randomAlphabetic(10);
		Web_GeneralFunctions.sendkeys(adPatSearch.getGovtID(Login_Doctor.driver, logger), input,"Entering random alphabets", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Assert.assertTrue(adPatSearch.isValueEntered(adPatSearch.getGovtID(Login_Doctor.driver, logger),input));
	}
	
	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch67() throws Exception {
		logger = Reports.extent.createTest("Verifying if numbers are entered in Govt ID");
		adPatSearch.getGovtID(Login_Doctor.driver, logger).clear();
		input= RandomStringUtils.randomNumeric(10);
		Web_GeneralFunctions.sendkeys(adPatSearch.getGovtID(Login_Doctor.driver, logger), input,"Entering random number", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Assert.assertTrue(adPatSearch.isValueEntered(adPatSearch.getGovtID(Login_Doctor.driver, logger),input));
	}
	
	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch68() throws Exception {
		logger = Reports.extent.createTest("Verifying if special characters are entered in Govt ID");
		adPatSearch.getGovtID(Login_Doctor.driver, logger).clear();
		input=SheetsAPI.getDataProperties(Web_Testbase.input+".SpecialChars");
	
		Web_GeneralFunctions.sendkeys(adPatSearch.getGovtID(Login_Doctor.driver, logger), input,"Entering special characters", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Assert.assertFalse(adPatSearch.isValueEntered(adPatSearch.getGovtID(Login_Doctor.driver, logger),input));
	}
	
	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch69() throws Exception {
		logger = Reports.extent.createTest("Verifying if results are obtained for valid Govt ID");
		adPatSearch.getGovtID(Login_Doctor.driver, logger).clear();
		input=SheetsAPI.getDataProperties(Web_Testbase.input+".GovtID");
		 
		Web_GeneralFunctions.sendkeys(adPatSearch.getGovtID(Login_Doctor.driver, logger), input,"Entering random alphabets", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(adPatSearch.getSearchBtn(Login_Doctor.driver, logger), "Clicking on Search button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(adPatSearch.getViewDetailsBtn(Login_Doctor.driver, logger), "Clicking on view details", Login_Doctor.driver, logger);
		String patienGovtID=adPatSearch.getGovtIDFromPatientDetails(Login_Doctor.driver,logger).getAttribute("value");
		Assert.assertTrue(patienGovtID.equals(input), "Verifying the govt id with search result");
		Web_GeneralFunctions.click(adPatSearch.getEmrMenu(Login_Doctor.driver, logger), "Clicking on Main menu", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(adPatSearch.getModuleFromLeftMenu(Login_Doctor.driver, logger,"Appointments"), "Clicking on Appointments", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(AdvancePatientSearch.getAdvanceSearchBtn(Login_Doctor.driver, logger), "Clicking to go back to advance search page", Login_Doctor.driver, logger);		
	}
	
	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch70() throws Exception {
		logger = Reports.extent.createTest("Verifying if characters are entered in Govt ID");
		adPatSearch.getGovtID(Login_Doctor.driver, logger).clear();
		input= RandomStringUtils.randomAlphabetic(15);
		Web_GeneralFunctions.sendkeys(adPatSearch.getGovtID(Login_Doctor.driver, logger), input,"Entering random alphabets", Login_Doctor.driver, logger);
		validateNoResultErrorMessage();
	}
	
	
	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch71() {
		logger = Reports.extent.createTest("Verifying max length limitation on Govt ID");
		adPatSearch.getGovtID(Login_Doctor.driver, logger).clear();
		String maxlength=adPatSearch.getGovtID(Login_Doctor.driver, logger).getAttribute("maxlength");
		Assert.assertTrue(maxlength==null,"No Max length specified");
	}
	
	@Test(groups= {"Regression","PatientAdvanceSearch","Sanity"})
	public synchronized void advanceSearch72() throws Exception {
		logger = Reports.extent.createTest("Verifying UHID and name combination");
		String name=SheetsAPI.getDataProperties(Web_Testbase.input+".PatientName");
		 
		Web_GeneralFunctions.sendkeys(adPatSearch.getSearchNameText(Login_Doctor.driver, logger), name, "Entering registered username", Login_Doctor.driver, logger);
		String validUHID=SheetsAPI.getDataProperties(Web_Testbase.input+".UHID");
		 
		Web_GeneralFunctions.sendkeys(adPatSearch.getUHIDTextField(Login_Doctor.driver, logger), validUHID,"Entering Alphabets ", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(adPatSearch.getSearchBtn(Login_Doctor.driver, logger), "Clicking on Search button", Login_Doctor.driver, logger);
		Assert.assertFalse(adPatSearch.getPatientNameSearchResultSelect(Login_Doctor.driver, logger).get(0).getText().equals(name),"Verifying the Name respective to UHID");
		Assert.assertTrue(adPatSearch.getUHIDDetail(Login_Doctor.driver, logger).getText().contains(validUHID), "Verifying valid UHID with result");
		Web_GeneralFunctions.clear(adPatSearch.getUHIDTextField(Login_Doctor.driver, logger),"clearing Alphabets ", Login_Doctor.driver, logger);
		Web_GeneralFunctions.clear(adPatSearch.getSearchNameText(Login_Doctor.driver, logger),"clearing Alphabets ", Login_Doctor.driver, logger);
		
	}
	
	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch73() throws Exception {
			logger = Reports.extent.createTest("Verifying the Update feature for User");
			String validUHID= SheetsAPI.getDataProperties(Web_Testbase.input+".UHID");
			Web_GeneralFunctions.sendkeys(adPatSearch.getUHIDTextField(Login_Doctor.driver, logger), validUHID,"Entering Alphabets ", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(adPatSearch.getSearchBtn(Login_Doctor.driver, logger), "Clicking on Search button", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(3);
			Web_GeneralFunctions.click(adPatSearch.getViewDetailsBtn(Login_Doctor.driver, logger), "Clicking on view details", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(3);
			String preclinicid=adPatSearch.getClinicIDInPatientDetails(Login_Doctor.driver,logger).getAttribute("value");
			
			//Update the Clinic ID
			String postClinicID=preclinicid+"_"+RandomStringUtils.randomNumeric(1);
			Web_GeneralFunctions.clearWebElement(adPatSearch.getClinicIDInPatientDetails(Login_Doctor.driver,logger), "Clearing ClinicID for update", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(adPatSearch.getClinicIDInPatientDetails(Login_Doctor.driver, logger),postClinicID,
					          "Entering Alphabets", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(adPatSearch.getSaveBtn(Login_Doctor.driver, logger), "Clicking on Save Button", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(adPatSearch.getEmrMenu(Login_Doctor.driver, logger), "Clicking on Main menu", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(adPatSearch.getModuleFromLeftMenu(Login_Doctor.driver, logger,"Appointments"), "Clicking on Appointments", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(AdvancePatientSearch.getAdvanceSearchBtn(Login_Doctor.driver, logger), "Clicking to go back to advance search page", Login_Doctor.driver, logger);
		
			//Search according to the Clinic ID
			Web_GeneralFunctions.sendkeys(adPatSearch.getClinicID(Login_Doctor.driver, logger), postClinicID, "Entering Clinic ID for search", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(adPatSearch.getSearchBtn(Login_Doctor.driver, logger), "Clicking on Search button", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(adPatSearch.getViewDetailsBtn(Login_Doctor.driver, logger), "Clicking on view details", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(3);
			Assert.assertTrue(adPatSearch.getClinicIDInPatientDetails(Login_Doctor.driver,logger).getAttribute("value").equals(postClinicID), "Verifying if the clinic id is updated");
		
			//Revert back the changes to Original Clinic ID
			Web_GeneralFunctions.clearWebElement(adPatSearch.getClinicIDInPatientDetails(Login_Doctor.driver,logger), "Clearing ClinicID for update", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(adPatSearch.getClinicIDInPatientDetails(Login_Doctor.driver, logger),preclinicid,          "Entering Alphabets", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(adPatSearch.getSaveBtn(Login_Doctor.driver, logger), "Clicking on Save Button", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(3);
			Web_GeneralFunctions.click(AdvancePatientSearch.getAdvanceSearchBtn(Login_Doctor.driver, logger), "Clicking on Advance Search", Login_Doctor.driver, logger);
		}
	
	@Test(groups= {"Regression","PatientAdvanceSearch","Sanity"})
	public synchronized void advanceSearch74() {
		logger = Reports.extent.createTest("Verifying the Search without anything entered");
		Web_GeneralFunctions.click(adPatSearch.getSearchBtn(Login_Doctor.driver, logger), "Clicking on Search Button",Login_Doctor.driver, logger);
		String enteronefield = adPatSearch.getToastMessage(Login_Doctor.driver, logger).getText();
		Assert.assertTrue("Please enter atleast one field".equals(enteronefield.trim()),"Actual Toast message with expected");
	}
	
	@Test(groups= {"Regression","PatientAdvanceSearch","Sanity"})
	public synchronized void advanceSearch75() throws Exception {
		logger = Reports.extent.createTest("Verifying the End Date field with special characters");
		String mobileNum= SheetsAPI.getDataProperties(Web_Testbase.input+".MobileNumber");
		Web_GeneralFunctions.sendkeys(adPatSearch.getMobileField(Login_Doctor.driver, logger), mobileNum,"Entering Alphabets ", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(adPatSearch.getSearchBtn(Login_Doctor.driver, logger), "Clicking on Search button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		
		//Verifying Patient Profile Picture Among the patient list
		for(WebElement ele:adPatSearch.getProfilePicFromSearchResult(Login_Doctor.driver, logger))
					 ele.isDisplayed();
				
		//Verifying Patient Name Among the patient list
		for(WebElement ele:adPatSearch.getPatientNameSearchResultSelect(Login_Doctor.driver, logger))
			 ele.isDisplayed();
		
		//Verifying Patient Age Among the patient list
		for(WebElement ele:adPatSearch.getPatientAgeSearchResultSelect(Login_Doctor.driver, logger))
			 ele.isDisplayed();
		
		//Verifying Patient UHID Among the patient list
		for(WebElement ele:adPatSearch.getUHIDFromSearchResult(Login_Doctor.driver, logger))
			 ele.isDisplayed();
		
		//Verifying Patient MobileNumber Among the patient list
		for(WebElement ele:adPatSearch.getMobileNoFieldFromSearchResult(Login_Doctor.driver, logger))
			ele.isDisplayed();
		adPatSearch.getMobileField(Login_Doctor.driver, logger).clear();
	}
	
	//Test Case advanceSearch80 covered as part of this
	@Test(groups= {"Regression","PatientAdvanceSearch","Sanity"})
	public synchronized void advanceSearch79() throws Exception {

		logger = Reports.extent.createTest("Verifying the Registered patient for callcenter doc and other role");
		input=SheetsAPI.getDataProperties(Web_Testbase.input+".ClinicPatient");
		Web_GeneralFunctions.sendkeys(adPatSearch.getUHIDTextField(Login_Doctor.driver, logger), input,"Entering Alphabets ", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(adPatSearch.getSearchBtn(Login_Doctor.driver, logger), "Clicking on Search button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		
		if(specificRole.equals(NEWCALCENTREDOC))
		{
			Assert.assertTrue(adPatSearch.getUHIDDetail(Login_Doctor.driver, logger).getText().contains(input),"Verifying if the result has same UHID like the entered value");
		}
		else
		{
			validateNoResultErrorMessage();
		}
		adPatSearch.getUHIDTextField(Login_Doctor.driver, logger).clear();
	}	
	
	@Test(groups= {"Regression","PatientAdvanceSearch","Sanity"})
	public synchronized void advanceSearch85() throws Exception {
		logger=Reports.extent.createTest("Verifying the Registered patient for callcenter doc and other role");
		input=SheetsAPI.getDataProperties(Web_Testbase.input+".ClinicPatientName");
		Web_GeneralFunctions.sendkeys(adPatSearch.getSearchNameText(Login_Doctor.driver, logger), input,"Entering Name from another clinic ", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(adPatSearch.getSearchBtn(Login_Doctor.driver, logger), "Clicking on Search button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		
		if(specificRole.equals(NEWCALCENTREDOC))
		{
			List<WebElement> resultNames = adPatSearch.getPatientNameSearchResultSelect(Login_Doctor.driver, logger);
			boolean res;
			if (input.contains(" ")) {
				String[] names=input.toLowerCase().split(" ");
				for (WebElement name : resultNames) {
					res=name.getText().toLowerCase().contains(names[0])||name.getText().toLowerCase().contains(names[1]);
					Assert.assertTrue(res,"List of Element Searched with Valid Name");
				}
			}
			else {		
				
			for (WebElement name : resultNames) {
				Assert.assertTrue(name.getText().toLowerCase().contains(input.toLowerCase()),"List of Element Searched with Valid Name");
			}
		}
			
		}
		else
		{
			validateNoResultErrorMessage();
		}
		adPatSearch.getSearchNameText(Login_Doctor.driver, logger).clear();
	}
	
	@Test(groups= {"Regression","PatientAdvanceSearch","Sanity"})
	public synchronized void advanceSearch87() throws Exception {
		logger=Reports.extent.createTest("Verifying the Registered patient for callcenter doc and other role");
		input=SheetsAPI.getDataProperties(Web_Testbase.input+".ClinicMobileNum");
		Web_GeneralFunctions.sendkeys(adPatSearch.getMobileField(Login_Doctor.driver, logger), input,"Entering Name from another clinic ", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(adPatSearch.getSearchBtn(Login_Doctor.driver, logger), "Clicking on Search button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		
		if(specificRole.equals(NEWCALCENTREDOC))
		{
			boolean res;
			List<WebElement> results=adPatSearch.getResultContainer(Login_Doctor.driver, logger);
			for(int i=0;i<results.size();i++)
				{

				List<WebElement>phonenum=adPatSearch.getMobileNoFromSearchResult(i+1,Login_Doctor.driver, logger);
				if(phonenum==null)
				{
					Assert.fail("No results are found");
				}
				else if(phonenum.size()==1)
					Assert.assertTrue(phonenum.get(0).getText().contains(input));
				else
				{
				res=phonenum.get(0).getText().contains(input)||phonenum.get(1).getText().contains(input);
				Assert.assertTrue(res);
				}
				}
		}
		else
		{
			validateNoResultErrorMessage();
		}
		adPatSearch.getMobileField(Login_Doctor.driver, logger).clear();
	}
	
	//This test case fails for Call centre doctor.
	@Test(groups= {"Regression","PatientAdvanceSearch","Sanity"})
	public synchronized void advanceSearch90() throws Exception {
		logger=Reports.extent.createTest("Verifying the Advance Search option for secondary number search within same clinic");
		input=SheetsAPI.getDataProperties(Web_Testbase.input+".SecMobNo");
		Web_GeneralFunctions.sendkeys(adPatSearch.getMobileField(Login_Doctor.driver, logger), input,"Entering Name from another clinic ", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(adPatSearch.getSearchBtn(Login_Doctor.driver, logger), "Clicking on Search button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
			
			boolean res;
			List<WebElement> results=adPatSearch.getResultContainer(Login_Doctor.driver, logger);
			for(int i=0;i<results.size();i++)
				{

				List<WebElement>phonenum=adPatSearch.getMobileNoFromSearchResult(i+1,Login_Doctor.driver, logger);
				if(phonenum==null)
				{
					Assert.fail("No results are found");
				}
				else if(phonenum.size()==1)
					Assert.assertTrue(phonenum.get(0).getText().contains(input));
				else
				{
				res=phonenum.get(0).getText().contains(input)||phonenum.get(1).getText().contains(input);
				Assert.assertTrue(res);
				}
				}
			adPatSearch.getMobileField(Login_Doctor.driver, logger).clear();
		}
	
	
	//This test case fails for Call centre doctor.
	@Test(groups= {"Regression","PatientAdvanceSearch","Sanity"})
	public synchronized void advanceSearch91() throws Exception {
		logger=Reports.extent.createTest("Verifying the Advance Search option for secondary number search from another clinic");
		input=SheetsAPI.getDataProperties(Web_Testbase.input+".ClinicSecMobNo");
		adPatSearch.getMobileField(Login_Doctor.driver, logger).clear();
		Web_GeneralFunctions.sendkeys(adPatSearch.getMobileField(Login_Doctor.driver, logger), input,"Entering Name from another clinic ", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(adPatSearch.getSearchBtn(Login_Doctor.driver, logger), "Clicking on Search button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		if(specificRole.equals(NEWCALCENTREDOC))
		{
			boolean res;
			List<WebElement> results=adPatSearch.getResultContainer(Login_Doctor.driver, logger); 
			for(int i=0;i<results.size();i++) {
				List<WebElement>phonenum=adPatSearch.getMobileNoFromSearchResult(i+1,Login_Doctor.driver, logger); 
				if(phonenum==null)
				{
					Assert.fail("No results are found");
				}
				else if(phonenum.size()==1)
						Assert.assertTrue(phonenum.get(0).getText().contains(input));
				else {
						 res=phonenum.get(0).getText().contains(input)||phonenum.get(1).getText().contains(input); 
						 Assert.assertTrue(res);
					}
			}
		}
		else {
			validateNoResultErrorMessage();
		}
			adPatSearch.getMobileField(Login_Doctor.driver, logger).clear();
			
		}
	//Test Case 89 covered under this test case
			@Test(groups= {"Regression","PatientAdvanceSearch"})
			public synchronized void advanceSearch88() throws Exception {
				logger=Reports.extent.createTest("Verifying the Advance Search option for Number registered in consumer site");

				AdvancePatientSearch.getAdvanceSearchBtn(Login_Doctor.driver, logger).click();
				Web_GeneralFunctions.wait(3);
				Web_GeneralFunctions.sendkeys(adPatSearch.getMobileField(Login_Doctor.driver, logger), PWALoginTest.mobileNum,"Entering mobile number", Login_Doctor.driver, logger);
				Web_GeneralFunctions.click(adPatSearch.getSearchBtn(Login_Doctor.driver, logger), "Clicking on Search button", Login_Doctor.driver, logger);
				Web_GeneralFunctions.wait(3);
				if(specificRole.equals(NEWCALCENTREDOC)) {
				boolean res;
				List<WebElement> results=adPatSearch.getResultContainer(Login_Doctor.driver, logger);
				for(int i=0;i<results.size();i++)
					{

					List<WebElement>phonenum=adPatSearch.getMobileNoFromSearchResult(i+1,Login_Doctor.driver, logger);
					if(phonenum==null)
					{
						Assert.fail("No results are found");
					}
					else if(phonenum.size()==1)
						Assert.assertTrue(phonenum.get(0).getText().contains(PWALoginTest.mobileNum));
					else
					{
					res=phonenum.get(0).getText().contains(PWALoginTest.mobileNum)||phonenum.get(1).getText().contains(PWALoginTest.mobileNum);
					Assert.assertTrue(res);
					}
					Assert.assertTrue(adPatSearch.getSignedUpUser(i+1,Login_Doctor.driver, logger).getText().contains("SU"));			
					}
				}
				else
				{
					String noResultFound = adPatSearch.getAdvanceSearchNoResultFoundText(Login_Doctor.driver, logger).getText();
					Assert.assertTrue("No results found".equals(noResultFound.trim()),"Actual No Result Found not matched with expected");
				}
				adPatSearch.getMobileField(Login_Doctor.driver, logger).clear();
			}
				
			@Test(groups= {"Regression","PatientAdvanceSearch"})
			public synchronized void advanceSearch89() throws Exception {
				logger=Reports.extent.createTest("Verifying the Advance Search option for Name registered in consumer site");

				AdvancePatientSearch.getAdvanceSearchBtn(Login_Doctor.driver, logger).click();
				Web_GeneralFunctions.wait(3);
				Web_GeneralFunctions.sendkeys(adPatSearch.getSearchNameText(Login_Doctor.driver, logger), PWALoginTest.name,"Entering Name", Login_Doctor.driver, logger);
				Web_GeneralFunctions.click(adPatSearch.getSearchBtn(Login_Doctor.driver, logger), "Clicking on Search Button",Login_Doctor.driver, logger);
				
				if(specificRole.equals(NEWCALCENTREDOC)) {
				List<WebElement> results=adPatSearch.getResultContainer(Login_Doctor.driver, logger); 
					for(int i=0;i<results.size();i++) {
						List<WebElement>phonenum=adPatSearch.getMobileNoFromSearchResult(i+1,Login_Doctor.driver, logger); 
						if(phonenum==null)
						{
							Assert.fail("No results are found");
						}
						else if(phonenum.get(0).getText().contains(PWALoginTest.mobileNum)) {
							Assert.assertTrue(adPatSearch.getPatientNameSearchResult(i+1,Login_Doctor.driver, logger).get(0).getText().equalsIgnoreCase(PWALoginTest.name),"Verifying if the Search matches with the name");
							Assert.assertTrue(adPatSearch.getSignedUpUser(i+1,Login_Doctor.driver, logger).getText().contains("SU"));
						}
					}
					
				}	
				else {
					String noResultFound = adPatSearch.getAdvanceSearchNoResultFoundText(Login_Doctor.driver, logger).getText();
					Assert.assertTrue("No results found".equals(noResultFound.trim()),"Actual No Result Found not matched with expected");
				}
				adPatSearch.getSearchNameText(Login_Doctor.driver, logger).clear();	
			}
			
	public synchronized void validateNoResultErrorMessage() throws Exception
	{
		Web_GeneralFunctions.click(adPatSearch.getSearchBtn(Login_Doctor.driver, logger), "Clicking on Search button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		String noResultFound = adPatSearch.getAdvanceSearchNoResultFoundText(Login_Doctor.driver, logger).getText();
		Assert.assertTrue("No results found".equals(noResultFound.trim()),"Actual No Result Found not matched with expected");
	}
	
	public synchronized String getCurrSysDate() {
		LocalDate date = LocalDate.now();
		DateTimeFormatter formatters = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		return date.format(formatters);
	}

	@DataProvider(name = "Patient name search data")
	public synchronized String[] getPatientSearchData() {
		String[] s = { "#123*", "*@" };
		return s;
	}

	@DataProvider(name = "Patient search data")
	public synchronized String[] getAgeSearchData() {
		String[] s = { "#*@", "acdfr@#" };
		return s;
	}

}

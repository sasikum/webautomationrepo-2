package com.tatahealth.EMR.Scripts.Login;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.API.libraries.SheetsAPI;
import com.tatahealth.DB.Scripts.OTP;
import com.tatahealth.EMR.pages.PatientAdvanceSearch.PWALogin;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;
import com.tatahealth.ReusableModules.Web_Testbase;

public class PWALoginTest {
	
	PWALogin pwa=new PWALogin();
	
	public static ExtentTest logger;
	public static WebDriver driver;

	public static String mobileNum="366"+RandomStringUtils.randomNumeric(7);
	public static String name="Ford"+RandomStringUtils.randomAlphabetic(3);
	public static String age=RandomStringUtils.randomNumeric(2);
	public static String getotp;
	public static String gender="Male";

	public static synchronized void LoginUser() throws Exception {

		driver = Web_Testbase.start("Reusable Driver", driver);
		logger = Reports.extent.createTest("Login to PWA");
	
		String url = SheetsAPI.getDataProperties(Web_Testbase.input + ".PWAURL");

		driver.get(url);
		Web_GeneralFunctions.wait(3);
		PWALogin pwa=new PWALogin();
		Web_GeneralFunctions.click(pwa.getLoginBtn(driver, logger), "Clicking on Login Button", driver, logger);
		Web_GeneralFunctions.wait(3);
		OTP otp=new OTP();
		Web_GeneralFunctions.sendkeys(pwa.getMobileNumber(PWALoginTest.driver, logger), mobileNum,"Sending Mobile Number", PWALoginTest.driver, logger);
		Web_GeneralFunctions.click(pwa.getContinueBtn(PWALoginTest.driver, logger), "Clicking on continue", PWALoginTest.driver, logger);
		Web_GeneralFunctions.wait(3);
		if(url.contains("https://caweb.tatahealth.com"))
		{
			getotp=otp.getQAOTP(mobileNum);
		}
		else getotp= otp.getStagingOTP(mobileNum);
		
		for(int i=0;i<getotp.length();i++)
		{
			pwa.getOTP(i+1,PWALoginTest.driver, logger).sendKeys(Character.toString(getotp.charAt(i)));
		}
		Web_GeneralFunctions.wait(5);
		if(!Web_GeneralFunctions.isDisplayed(pwa.getName(PWALoginTest.driver, logger)))
		{
			pwa.getHomeBtn(driver, logger).isDisplayed();
		}
		else
		{
			Web_GeneralFunctions.sendkeys(pwa.getName(PWALoginTest.driver, logger), name, "Entering Name", PWALoginTest.driver, logger);
			Web_GeneralFunctions.sendkeys(pwa.getAge(PWALoginTest.driver, logger), age, "Entering Age", PWALoginTest.driver, logger);
			Web_GeneralFunctions.sendkeys(pwa.getGender(PWALoginTest.driver, logger), gender, "Entering Name", PWALoginTest.driver, logger);
			Web_GeneralFunctions.click(pwa.getContinueBtn(PWALoginTest.driver, logger), "Clicking on Continue Btn", PWALoginTest.driver, logger);
			Web_GeneralFunctions.wait(10);	
		}
	}
	
	public  static synchronized  void LogoutUser(){
			PWALogin pwa=new PWALogin();
			Web_GeneralFunctions.click(pwa.getSignedUpUserBtn(name, driver, logger), "Clicking on Name", driver, logger);
			Web_GeneralFunctions.click(pwa.myAccountBtn(driver, logger), "Clicking on My Account", driver, logger);
			Web_GeneralFunctions.click(pwa.mySettingBtn(driver, logger), "Clicking on settings", driver, logger);
			Web_GeneralFunctions.click(pwa.logoutBtn(driver, logger), "Clicking on logout", driver, logger);
			Web_GeneralFunctions.click(pwa.confirmLogout(driver, logger), "Clicking on logout for confirmation", driver, logger);
			driver.manage().deleteAllCookies();
			driver.close();
			driver.quit();
		}
	 
	
}

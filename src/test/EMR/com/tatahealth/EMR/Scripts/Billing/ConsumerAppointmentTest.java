package com.tatahealth.EMR.Scripts.Billing;

import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.tatahealth.API.Billing.PaymentDetails;
import com.tatahealth.API.Core.CCAvenueDetails;
import com.tatahealth.API.libraries.ExcelIntegration;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.ConsumerAppointment.Billing.GenerateBillCases;
import com.tatahealth.EMR.Scripts.Consultation.ConsultationTest;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.AppointmentDashboard.AllSlotsPage;
import com.tatahealth.EMR.pages.Billing.Billing;
import com.tatahealth.EMR.pages.Consultation.ConsultationPage;
import com.tatahealth.EMR.pages.Login.LoginPage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class ConsumerAppointmentTest {
	BillingTest bs = new BillingTest();
	Billing bill = new Billing();
	LoginPage loginPage = new LoginPage();
	MasterAdministrationTest masterAdmin = new MasterAdministrationTest();
	ConsultationTest appt = new ConsultationTest();
	AllSlotsPage ASpage = new AllSlotsPage();
	ConsultationPage consultation = new ConsultationPage();
	GenerateBillCases gc = new GenerateBillCases();
	PreBillingTest pb = new PreBillingTest();
	PostBillingTest psb = new PostBillingTest();
	StandardBillingTest sb = new StandardBillingTest();
	public static ExtentTest logger;
	public static String paymentMode;
	public static Integer slotNumber;
	public static String consumerName;
	public static double serviceAmount;
	public static String Paid;

	@BeforeClass(alwaysRun = true)
	public static void beforeClass() throws Exception {
		Reports.reports();
		Login_Doctor.executionName = "ConsumerAppointmnet";
		BillingTest.doctorName="Saket";
		Login_Doctor.LoginTestwithDiffrentUser(BillingTest.doctorName);
	}

	@AfterClass(alwaysRun = true)
	public static void afterClass() throws Exception {
		Login_Doctor.LogoutTest();
		Reports.extent.flush();
	}

	public void appointmentConsumerData() throws Exception {
		String file = "./appointment_details.xlsx";
		FileInputStream stream = new FileInputStream(new File(file));
		Workbook book = new XSSFWorkbook(stream);
		int sheetRow = 0;
		Sheet sheetNum = book.getSheetAt(sheetRow);
		Iterator<Row> iterRow = sheetNum.iterator();
		iterRow.next();
		iterRow.next();
		while (iterRow.hasNext()) {
			PaymentDetails p = new PaymentDetails();
			Row row = iterRow.next();
			Iterator<Cell> iterCell = row.iterator();
			paymentMode = iterCell.next().toString().trim();
			slotNumber = Integer.parseInt(iterCell.next().toString().trim());
			consumerName = iterCell.next().toString().trim();
			serviceAmount = Double.parseDouble(iterCell.next().toString().trim());
			iterCell.next();
			iterCell.next();
			iterCell.next();
			Paid = iterCell.next().toString().trim();
			stream.close();
			book.close();

		}

	}

	public synchronized void preBillingMaster() throws Exception {
		logger = Reports.extent.createTest("EMR_Pre Billing Master Validation");
		bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		bill.getEmrTopMenu(Login_Doctor.driver, logger).click();
		masterAdmin.masterAdministrationBillType("Pre-Billing");
		Web_GeneralFunctions.wait(4);
		Login_Doctor.Logout();
	}

	public synchronized void postBillingMaster() throws Exception {
		logger = Reports.extent.createTest("EMR_Pre Billing Master Validation");
		bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		bill.getEmrTopMenu(Login_Doctor.driver, logger).click();
		masterAdmin.masterAdministrationBillType("Post-Billing");
		Web_GeneralFunctions.wait(4);
		Login_Doctor.Logout();
	}

	public synchronized void standardBillingMaster() throws Exception {
		logger = Reports.extent.createTest("EMR_Pre Billing Master Validation");
		bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		bill.getEmrTopMenu(Login_Doctor.driver, logger).click();
		masterAdmin.masterAdministrationBillType("Standard Billing");
		Web_GeneralFunctions.wait(4);
		Login_Doctor.Logout();
	}

	/* Test case:EMR_Prebill_11,Biiling_C_P_14 */
	@Test(priority = 52)
	public void preBillingConsumerAppointmentValidation() throws Exception {
		logger = Reports.extent.createTest("preBilling Consumer Appointment Validation");
		preBillingMaster();
		consumerBookAppointment("Coupon + Fit Coin + Pay At Clinic");
		doctorLogin();
		consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(5);
		psb.billingAppointmentConsumerValidation();
	}

	/* Test case:EMR_Post billing_38,Biiling_C_P_13 */
	@Test(priority = 53)
	public void postBillingConsumerAppointmentValidation() throws Exception {
		logger = Reports.extent.createTest("postBilling Consumer Appointment Validation");
		postBillingMaster();
		consumerBookAppointment("Coupon + Fit Coin + Pay At Clinic");
		doctorLogin();
		consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(5);
		psb.billingAppointmentConsumerValidation();
	}

	/*
	 * Test case:EMR_Standard_25*,Biiling_C_P_15/ /*EMR_Standard_28 user specific
	 */
	@Test(priority = 54)
	public void standardBillingConsumerAppointmentValidation() throws Exception {
		logger = Reports.extent.createTest("standardBilling Consumer Appointment Validation");
		standardBillingMaster();
		consumerBookAppointment("Coupon + Fit Coin + Pay At Clinic");
		doctorLogin();
		consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(5);
		psb.billingAppointmentConsumerValidation();
	}

	/* Test case:Biiling_C_P_16 */
	@Test(priority = 55)
	public void checkoutBillingConsumerAppointmentValidation() throws Exception {
		logger = Reports.extent.createTest("checkout Billing Consumer Appointment Validation");
		consumerBookAppointment("Coupon + Fit Coin + Pay At Clinic");
		bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(5);
		bs.rowCount = 0;
		appt.getAppointmentDropdown();
		appt.appointmentBookedStatus();
		appt.bookedAppointmentOptionValidation();
		appt.getAppointmentDropDownValue(3, "Click on Consult");
		appt.afterConsultClickValidation();
		appt.checkOutAndBillPatientConsultPageClick();
		bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		bs.billPageDoctorAndPatientFieldValidation();
		bs.billingCreateBillServiceSectionColumnDisplay();
		bs.billConsumerServiceReadOnlyValiadtion();
		bs.billConsumerCFPcServiceValidation();
		bill.getEmrCreateBillCancelButton(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(5);
		appt.getAppointmentDropdown();
		appt.appointmentCheckedOutStatus();
		appt.bookedAppointmentOptionAfterConsultCancelBillValidation();
		appt.getAppointmentDropDownValue(2, "Click on Genrate Bill");
		bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		bs.billPageDoctorAndPatientFieldValidation();
		bs.billingCreateBillServiceSectionColumnDisplay();
		bs.billConsumerServiceReadOnlyValiadtion();
		bs.billConsumerCFPcServiceValidation();
	}

	/* Test case:Biiling_C_P_17 */
	@Test(priority = 56)
	public void cancelBillingConsumerAppointmentValidation() throws Exception {
		logger = Reports.extent.createTest("Cancel Billing Consumer Appointment Validation");
		consumerBookAppointment("Coupon + Fit Coin + Pay At Clinic");
		bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(5);
		bs.rowCount = 0;
		appt.getAppointmentDropdown();
		appt.bookedAppointmentOptionValidation();
		appt.getAppointmentDropDownValue(2, "Click on checkin and generate bill");
		bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		bs.billPageDoctorAndPatientFieldValidation();
		bs.billingCreateBillServiceSectionColumnDisplay();
		bs.billConsumerServiceReadOnlyValiadtion();
		bs.billConsumerCFPcServiceValidation();
		bill.getEmrCreateBillCancelButton(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(5);
		appt.getAppointmentDropdown();
		appt.bookedAppointmentOptionAfterCheckinOrCheckinGenerateBillCancelValidation();
		appt.getAppointmentDropDownValue(2, "Click on  Consult");
		appt.afterConsultClickValidation();
		appt.checkOutAndBillPatientConsultPageClick();
		bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		bs.billPageDoctorAndPatientFieldValidation();
		bs.billingCreateBillServiceSectionColumnDisplay();
		bs.billConsumerServiceReadOnlyValiadtion();
		bs.billConsumerCFPcServiceValidation();
	}

	@Test(priority = 57)
	public void saveDraftBillingConsumerAppointmentValidation() throws Exception {
		logger = Reports.extent.createTest("Save Draft Billing Consumer Appointment Validation");
		bs.rowCount = 0;
		bs.expectedNetTotalAmount = 0;
		consumerBookAppointment("Pay At Clinic");
		bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(5);
		appt.getAppointmentDropdown();
		appt.appointmentBookedStatus();
		appt.appointmentPaymentModePcStatusBooked();
		appt.bookedAppointmentOptionValidation();
		appt.getAppointmentDropDownValue(2, "Click on checkin and Generate Bill");
		bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		bs.billPageDoctorAndPatientFieldValidation();
		bs.billingCreateBillServiceSectionColumnDisplay();
		bs.billConsumerPcServiceValidation();
		bs.billDefaultServiceLevelChangeValidation();
		bs.deleteService();
		bs.emrAppointmentGenerateBillSaveDraftValidation();
		bill.getEmrCreateBillSaveDraftButton(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(5);
		consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(5);
		appt.getAppointmentDropdown();
		appt.bookedAppointmentOptionAfterCheckinOrCheckinGenerateBillCancelValidation();
		appt.getAppointmentDropDownValue(3, "Click on Generate Bill");
		bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		bs.billPageDoctorAndPatientFieldValidation();
		bs.billSaveServiceValidation();
	}

	public void consumerOBillingPageValidation() throws Exception {
		logger = Reports.extent.createTest("consumer Online Billing Page Validation");
		bs.rowCount = 0;
		bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		bs.billPageDoctorAndPatientFieldValidation();
		bs.billingCreateBillServiceSectionColumnDisplay();
		bs.billConsumerServiceReadOnlyValiadtion();
		bs.billConsumerOServiceValidation();
		try {
			bs.deleteService();
			Assert.assertTrue(false, "Delete Button Displayed");
		} catch (Exception e) {
			logger.log(Status.PASS, MarkupHelper.createLabel("No Delete Button Displayed", ExtentColor.GREEN));
		}
		bs.consumerAppointmentGenerateBillValidation();
	}

	public void consumerFBillingPageValidation() throws Exception {
		logger = Reports.extent.createTest("consumer Fitcoin Billing Page Validation");
		bs.rowCount = 0;
		bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		bs.billPageDoctorAndPatientFieldValidation();
		bs.billingCreateBillServiceSectionColumnDisplay();
		bs.billConsumerServiceReadOnlyValiadtion();
		bs.billConsumerFServiceValidation();
		try {
			bs.deleteService();
			Assert.assertTrue(false, "Delete Button Displayed");
		} catch (Exception e) {
			logger.log(Status.PASS, MarkupHelper.createLabel("No Delete Button Displayed", ExtentColor.GREEN));
		}
		bs.consumerAppointmentGenerateBillValidation();
	}

	public void consumerCBillingPageValidation() throws Exception {
		logger = Reports.extent.createTest("consumer Fitcoin Billing Page Validation");
		bs.rowCount = 0;
		bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		bs.billPageDoctorAndPatientFieldValidation();
		bs.billingCreateBillServiceSectionColumnDisplay();
		bs.billConsumerServiceReadOnlyValiadtion();
		bs.billConsumerCServiceValidation();
		try {
			bs.deleteService();
			Assert.assertTrue(false, "Delete Button Displayed");
		} catch (Exception e) {
			logger.log(Status.PASS, MarkupHelper.createLabel("No Delete Button Displayed", ExtentColor.GREEN));
		}
		bs.consumerAppointmentGenerateBillValidation();
	}

	public void consumerPBillingPageValidation() throws Exception {
		logger = Reports.extent.createTest("consumer Fitcoin Billing Page Validation");
		bs.rowCount = 0;
		bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		bs.billPageDoctorAndPatientFieldValidation();
		bs.billingCreateBillServiceSectionColumnDisplay();
		bs.billConsumerServiceReadOnlyValiadtion();
		bs.billConsumerPServiceValidation();
		try {
			bs.deleteService();
			Assert.assertTrue(false, "Delete Button Displayed");
		} catch (Exception e) {
			logger.log(Status.PASS, MarkupHelper.createLabel("No Delete Button Displayed", ExtentColor.GREEN));
		}
		bs.consumerAppointmentGenerateBillValidation();
	}

	public void consumerPcBillingPageValidation() throws Exception {
		logger = Reports.extent.createTest("consumer Pay at Clinic Billing Page Validation");
		bs.rowCount = 0;
		bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		bs.billPageDoctorAndPatientFieldValidation();
		bs.billingCreateBillServiceSectionColumnDisplay();
		bs.billConsumerPcServiceValidation();
		bs.billDefaultServiceLevelChangeValidation();
		bs.deleteService();
		bs.emrAppointmentGenerateBillValidation();
	}

	public void consumerPcBillingPageNoServiceValidation() throws Exception {
		logger = Reports.extent.createTest("consumer Pay at Clinic Billing Page Validation No Service");
		bs.rowCount = 0;
		bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		bs.billPageDoctorAndPatientFieldValidation();
		bs.billingCreateBillServiceSectionColumnDisplay();
		bs.billConsumerPcServiceValidation();
		bill.getEmrCreateBillActionDeleteButton(Login_Doctor.driver, logger, bs.rowCount).isDisplayed();
		bs.consumerAppointmentNoServiceGenerateBillValidation();
	}

	public void consumerCOBillingPageValidation() throws Exception {
		logger = Reports.extent.createTest("consumer Coupon+Online Billing Page Validation");
		bs.rowCount = 0;
		bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		bs.billPageDoctorAndPatientFieldValidation();
		bs.billingCreateBillServiceSectionColumnDisplay();
		bs.billConsumerServiceReadOnlyValiadtion();
		bs.billConsumerCOServiceValidation();
		try {
			bs.deleteService();
			Assert.assertTrue(false, "Delete Button Displayed");
		} catch (Exception e) {
			logger.log(Status.PASS, MarkupHelper.createLabel("No Delete Button Displayed", ExtentColor.GREEN));
		}
		bs.consumerAppointmentGenerateBillValidation();
	}

	public void consumerFPcBillingPageValidation() throws Exception {
		logger = Reports.extent.createTest("consumer Fitcoin+Pay at Clinic Billing Page Validation");
		bs.rowCount = 0;
		bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		bs.billPageDoctorAndPatientFieldValidation();
		bs.billingCreateBillServiceSectionColumnDisplay();
		bs.billConsumerServiceReadOnlyValiadtion();
		bs.billConsumerFPcServiceValidation();
		try {
			bs.deleteService();
			Assert.assertTrue(false, "Delete Button Displayed");
		} catch (Exception e) {
			logger.log(Status.PASS, MarkupHelper.createLabel("No Delete Button Displayed", ExtentColor.GREEN));
		}
		bs.consumerAppointmentGenerateBillValidation();
	}

	public void consumerFPcBillingPageNoServiceValidation() throws Exception {
		logger = Reports.extent.createTest("consumer Fitcoin+Pay at Clinic Billing Page Validation No Service");
		bs.rowCount = 0;
		bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		bs.billPageDoctorAndPatientFieldValidation();
		bs.billingCreateBillServiceSectionColumnDisplay();
		bs.billConsumerServiceReadOnlyValiadtion();
		bs.billConsumerFPcServiceValidation();
		try {
			bs.deleteService();
			Assert.assertTrue(false, "Delete Button Displayed");
		} catch (Exception e) {
			logger.log(Status.PASS, MarkupHelper.createLabel("No Delete Button Displayed", ExtentColor.GREEN));
		}
		bs.consumerAppointmentNoServiceGenerateBillValidation();
	}

	public void consumerCFBillingPageValidation() throws Exception {
		logger = Reports.extent.createTest("consumer Coupon+Fitcoin Billing Page Validation");
		bs.rowCount = 0;
		bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		bs.billPageDoctorAndPatientFieldValidation();
		bs.billingCreateBillServiceSectionColumnDisplay();
		bs.billConsumerServiceReadOnlyValiadtion();
		bs.billConsumerCFServiceValidation();
		try {
			bs.deleteService();
			Assert.assertTrue(false, "Delete Button Displayed");
		} catch (Exception e) {
			logger.log(Status.PASS, MarkupHelper.createLabel("No Delete Button Displayed", ExtentColor.GREEN));
		}
		bs.consumerAppointmentGenerateBillValidation();
	}

	public void consumerCPcBillingPageValidation() throws Exception {
		logger = Reports.extent.createTest("consumer Coupon+Pay at Clinic Billing Page Validation");
		bs.rowCount = 0;
		bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		bs.billPageDoctorAndPatientFieldValidation();
		bs.billingCreateBillServiceSectionColumnDisplay();
		bs.billConsumerServiceReadOnlyValiadtion();
		bs.billConsumerCPcServiceValidation();
		try {
			bs.deleteService();
			Assert.assertTrue(false, "Delete Button Displayed");
		} catch (Exception e) {
			logger.log(Status.PASS, MarkupHelper.createLabel("No Delete Button Displayed", ExtentColor.GREEN));
		}
		bs.consumerAppointmentGenerateBillValidation();
	}

	public void consumerCPcBillingPageNoServiceValidation() throws Exception {
		logger = Reports.extent.createTest("consumer Coupon+Pay at Clinic Billing Page Validation No Service");
		bs.rowCount = 0;
		bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		bs.billPageDoctorAndPatientFieldValidation();
		bs.billingCreateBillServiceSectionColumnDisplay();
		bs.billConsumerServiceReadOnlyValiadtion();
		bs.billConsumerCPcServiceValidation();
		try {
			bs.deleteService();
			Assert.assertTrue(false, "Delete Button Displayed");
		} catch (Exception e) {
			logger.log(Status.PASS, MarkupHelper.createLabel("No Delete Button Displayed", ExtentColor.GREEN));
		}
		bs.consumerAppointmentNoServiceGenerateBillValidation();
	}

	public void consumerPPcBillingPageValidation() throws Exception {
		logger = Reports.extent.createTest("consumer Package+Pay at Clinic Billing Page Validation");
		bs.rowCount = 0;
		bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		bs.billPageDoctorAndPatientFieldValidation();
		bs.billingCreateBillServiceSectionColumnDisplay();
		bs.billConsumerServiceReadOnlyValiadtion();
		bs.billConsumerPPcServiceValidation();
		try {
			bs.deleteService();
			Assert.assertTrue(false, "Delete Button Displayed");
		} catch (Exception e) {
			logger.log(Status.PASS, MarkupHelper.createLabel("No Delete Button Displayed", ExtentColor.GREEN));
		}
		bs.consumerAppointmentGenerateBillValidation();
	}

	public void consumerPPcBillingPageNoServiceValidation() throws Exception {
		logger = Reports.extent.createTest("consumer Package+Pay at Clinic Billing Page Validation No Service");
		bs.rowCount = 0;
		bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		bs.billPageDoctorAndPatientFieldValidation();
		bs.billingCreateBillServiceSectionColumnDisplay();
		bs.billConsumerServiceReadOnlyValiadtion();
		bs.billConsumerPPcServiceValidation();
		try {
			bs.deleteService();
			Assert.assertTrue(false, "Delete Button Displayed");
		} catch (Exception e) {
			logger.log(Status.PASS, MarkupHelper.createLabel("No Delete Button Displayed", ExtentColor.GREEN));
		}
		bs.consumerAppointmentNoServiceGenerateBillValidation();
	}

	public void consumerFOBillingPageValidation() throws Exception {
		logger = Reports.extent.createTest("consumer Fitcoin+Online Billing Page Validation");
		bs.rowCount = 0;
		bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		bs.billPageDoctorAndPatientFieldValidation();
		bs.billingCreateBillServiceSectionColumnDisplay();
		bs.billConsumerServiceReadOnlyValiadtion();
		bs.billConsumerFOServiceValidation();
		try {
			bs.deleteService();
			Assert.assertTrue(false, "Delete Button Displayed");
		} catch (Exception e) {
			logger.log(Status.PASS, MarkupHelper.createLabel("No Delete Button Displayed", ExtentColor.GREEN));
		}
		bs.consumerAppointmentGenerateBillValidation();
	}

	public void consumerPOBillingPageValidation() throws Exception {
		logger = Reports.extent.createTest("consumer Package+Online Billing Page Validation");
		bs.rowCount = 0;
		bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		bs.billPageDoctorAndPatientFieldValidation();
		bs.billingCreateBillServiceSectionColumnDisplay();
		bs.billConsumerServiceReadOnlyValiadtion();
		bs.billConsumerPOServiceValidation();
		try {
			bs.deleteService();
			Assert.assertTrue(false, "Delete Button Displayed");
		} catch (Exception e) {
			logger.log(Status.PASS, MarkupHelper.createLabel("No Delete Button Displayed", ExtentColor.GREEN));
		}
		bs.consumerAppointmentGenerateBillValidation();
	}

	public void consumerCFPcBillingPageValidation() throws Exception {
		logger = Reports.extent.createTest("consumer Coupon+Fitcoin+Pay at Clinic Billing Page Validation");
		bs.rowCount = 0;
		bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		bs.billPageDoctorAndPatientFieldValidation();
		bs.billingCreateBillServiceSectionColumnDisplay();
		bs.billConsumerServiceReadOnlyValiadtion();
		bs.billConsumerCFPcServiceValidation();
		try {
			bs.deleteService();
			Assert.assertTrue(false, "Delete Button Displayed");
		} catch (Exception e) {
			logger.log(Status.PASS, MarkupHelper.createLabel("No Delete Button Displayed", ExtentColor.GREEN));
		}
		bs.consumerAppointmentGenerateBillValidation();
	}

	public void consumerCFPcBillingPageNoServiceValidation() throws Exception {
		logger = Reports.extent.createTest("consumer Coupon+Fitcoin+Pay at Clinic Billing Page Validation No Service");
		bs.rowCount = 0;
		bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		bs.billPageDoctorAndPatientFieldValidation();
		bs.billingCreateBillServiceSectionColumnDisplay();
		bs.billConsumerServiceReadOnlyValiadtion();
		bs.billConsumerCFPcServiceValidation();
		try {
			bs.deleteService();
			Assert.assertTrue(false, "Delete Button Displayed");
		} catch (Exception e) {
			logger.log(Status.PASS, MarkupHelper.createLabel("No Delete Button Displayed", ExtentColor.GREEN));
		}
		bs.consumerAppointmentNoServiceGenerateBillValidation();
	}

	public void consumerCFOBillingPageValidation() throws Exception {
		logger = Reports.extent.createTest("consumer Coupon+Fitcoin+Online Billing Page Validation");
		bs.rowCount = 0;
		bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		bs.billPageDoctorAndPatientFieldValidation();
		bs.billingCreateBillServiceSectionColumnDisplay();
		bs.billConsumerServiceReadOnlyValiadtion();
		bs.billConsumerCFOServiceValidation();
		try {
			bs.deleteService();
			Assert.assertTrue(false, "Delete Button Displayed");
		} catch (Exception e) {
			logger.log(Status.PASS, MarkupHelper.createLabel("No Delete Button Displayed", ExtentColor.GREEN));
		}
		bs.consumerAppointmentGenerateBillValidation();
	}

	public void consumerBookAppointment(String bookType) throws Exception {
		logger = Reports.extent.createTest("Consumer Book Appointment " + bookType + "");
		gc.appointmentConsumer(bookType);
		appointmentConsumerData();
		Integer ps = slotNumber - 2;
		appt.prevSlotId = String.valueOf(ps);
		appt.slotId = String.valueOf(slotNumber);
		bs.patientName = consumerName.trim();
		bs.doctorName = BillingTest.doctorName;
	}

	public void consumerViewBillAndCancelBillAppointmentValidation(String paymentCheck) throws Exception {
		bs.billingPageLaunch();
		bill.getEmrBillTab(Login_Doctor.driver, logger, "View Bill").click();
		bs.viewBillSearchAllfieldDisplayValidation();
		bill.getEmrViewBillPatientNameSearchTextBox(Login_Doctor.driver, logger).clear();
		bill.getEmrViewBillPatientNameSearchTextBox(Login_Doctor.driver, logger).sendKeys(bs.patientName);
		bs.searchBillButtonClick();
		bs.viewBillSearchResultColumnDisplayValidation();
		bs.viewBillSearchResultValiadtion(paymentCheck.trim());
		Web_GeneralFunctions.wait(5);
		bill.getEmrTopMenuElement(Login_Doctor.driver, logger, "Appointments").click();
		Web_GeneralFunctions.wait(5);
		consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(5);
		appt.getAppointmentDropdown();
		appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		appt.getAppointmentDropDownValue(3, "Click on Cancel Bill");
		String successMessage = bill.getEmrBillErrorMessage(Login_Doctor.driver, logger).getText().trim();
		Assert.assertTrue("Bill Cancelled Successfully".equals(successMessage), "Cancel bill Message not expected");
		Web_GeneralFunctions.wait(5);
		appt.getAppointmentDropdown();
		appt.bookedAppointmentOptionAfterCheckinOrCheckinGenerateBillCancelValidation();
		appt.getAppointmentDropDownValue(3, "Click on Generate Bill");
		bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		bs.billPageDoctorAndPatientFieldValidation();
	}

	public void doctorLogin() throws Exception {
		Login_Doctor.LoginTestwithoutNewInstance(BillingTest.doctorName);
	}
}
